import numpy as np
import os
import shutil
import tempfile
import unittest

from chainer import testing
from chainer.testing import attr

from chainerpcl.datasets import KITTIDataset
from chainerpcl.datasets.kitti.kitti_utils import kitti_labels
from chainerpcl.utils import assert_is_semantic_segmentation_dataset
from chainerpcl.utils.testing.assertions.assert_is_image import assert_is_image
from chainerpcl.utils import write_image


@testing.parameterize(
    {
        'date': '2011_09_26', 
        'driveNo': '0001', 
        'color': True,
        'sync': True
    },
    {
        'date': '2011_09_26', 
        'driveNo': '0001', 
        'color': True,
        'sync': False
    },
    {
        'date': '2011_09_26', 
        'driveNo': '0001', 
        'color': True,
        'sync': True
    },
    {
        'date': '2011_09_26', 
        'driveNo': '0017', 
        'color': True,
        'sync': True
    },
    {
        'date': '2011_09_28', 
        'driveNo': '0001', 
        'color': True,
        'sync': True
    },
    {
        'date': '2011_10_03', 
        'driveNo': '0047', 
        'color': True,
        'sync': True
    },
)


class TestKITTISemanticSegmentationDataset(unittest.TestCase):

    def setUp(self):
        self.dataset = KITTISemanticSegmentationDataset(
                                                        date=self.date,
                                                        driveNo=self.driveNo,
                                                        color=self.color,
                                                        sync=self.sync)

    @attr.slow
    @attr.disk
    def test_kitti_dataset(self):
        assert_is_semantic_segmentation_dataset(
            self.dataset, len(KITTI_label_names),
            n_example=10)


class TestKITTITestImageDataset(unittest.TestCase):

    def setUp(self):
        self.dataset = KITTIDataset(
                                    date=self.date,
                                    driveNo=self.driveNo,
                                    imgNo=self.imgNo, 
                                    sync=self.sync)


    @attr.slow
    @attr.disk
    def test_kitti_dataset(self):
        indices = np.random.permutation(np.arange(len(self.dataset)))
        for i in indices[:10]:
            img = self.dataset[i]
            assert_is_image(img, color=True)


testing.run_module(__name__, __file__)

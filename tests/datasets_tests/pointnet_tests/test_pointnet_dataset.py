import unittest

from chainer import testing
from chainer.testing import attr

from chainerpcl.datasets import chainerpcl_label_names
from chainerpcl.datasets import PointnetDataset
from chainerpcl.utils import assert_is_semantic_segmentation_dataset


@testing.parameterize(
    {'split': 'train'},
    {'split': 'val'},
    {'split': 'test'}
)
class TestPointnetDataset(unittest.TestCase):

    def setUp(self):
        self.dataset = PointnetDataset(split=self.split)

    @attr.slow
    def test_pointnet_dataset(self):
        assert_is_semantic_segmentation_dataset(self.dataset, len(camvid_label_names), n_example=10)


testing.run_module(__name__, __file__)

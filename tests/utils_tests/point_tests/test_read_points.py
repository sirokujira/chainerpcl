import numpy as np
# from PIL import Image
import pcl
import tempfile
import unittest

from chainer import testing

from chainerpcl.utils import read_points


@testing.parameterize(*testing.product({
    'size': [(512, 3)],
    'color': [True, False],
    'suffix': ['bmp', 'jpg', 'png'],
    'dtype': [np.float32, np.uint8, bool],
}))
class TestReadPointCloud(unittest.TestCase):

    def setUp(self):
        self.file = tempfile.NamedTemporaryFile(
            suffix='.' + self.suffix, delete=False)
        self.path = self.file.name

        if self.color:
            self.points = np.random.randint(
                0, 255, size=(3,) + self.size, dtype=np.uint8)
            # Image.fromarray(self.img.transpose(1, 2, 0)).save(self.path)
            pcl.save(self.points, self.path)
        else:
            self.points = np.random.randint(
                0, 255, size=(1,) + self.size, dtype=np.uint8)
            # Image.fromarray(self.point[0]).save(self.path)
            pcl.save(self.points, self.path)


    def test_read_point_as_color(self):
        if self.dtype == np.float32:
            points = read_points(self.path)
        else:
            points = read_points(self.path, dtype=self.dtype)

        self.assertEqual(points.shape, (3,) + self.size)
        self.assertEqual(points.dtype, self.dtype)

        if self.suffix in {'pcd', 'ply', 'obj'}:
            np.testing.assert_equal(
                points,
                np.broadcast_to(self.point, (3,) + self.size).astype(self.dtype))


    def test_read_point_mutable(self):
        point = read_image(self.path)
        point[:] = 0
        np.testing.assert_equal(point, 0)


testing.run_module(__name__, __file__)

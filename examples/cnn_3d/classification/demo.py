import argparse
# https://matplotlib.org/examples/mplot3d/scatter3d_demo.html
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import chainer

from chainerpcl.datasets import shapenet
from chainerpcl.links.model.pointnet import PointNetCls
from chainerpcl import utils
from chainerpcl.visualizations import vis_pointcloud, vis_pointcloud_pcl
from chainerpcl.visualizations import vis_label_3d
from chainer import serializers

import numpy as np
import onnx_chainer

shapenetCorev2_label_names = (
    'Airplane',
    'Bag',
    'Cap',
    'Car',
    'Chair',
    'Earphone',
    'Guitar',
    'Knife',
    'Lamp',
    'Laptop',
    'Motorbike',
    'Mug',
    'Pistol',
    'Rocket',
    'Skateboard',
    'Table'
)

shapenetCorev2_label_colors = (
    (128, 128, 128),
    (128, 0, 0),
    (192, 192, 128),
    (128, 64, 128),
    (60, 40, 222),
    (128, 128, 0),
    (192, 128, 128),
    (64, 64, 128),
    (64, 0, 128),
    (64, 64, 0),
    (0, 128, 192),
    (0, 32, 0),
    (0, 0, 32),
    (0, 96, 0),
    (0, 160, 96),
    (0, 160, 160)
)


def main():
    chainer.config.train = False

    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', type=int, default=-1)
    parser.add_argument('--pretrained_model', default='cls_model_4.npz')
    # parser.add_argument('--pointnum', default=2500)
    # parser.add_argument('--points', default='bunny.pcd')
    # parser.add_argument('--points', default='ism_train_cat.pcd')
    # parser.add_argument('--points', default='model_000056.obj')
    parser.add_argument('--points', default='data1.pcd')
    # parser.add_argument('--points', default='data2.pcd')
    # parser.add_argument('--points', default='data3.pcd')
    # parser.add_argument('--points', default='data4.pcd')
    # parser.add_argument('--points', default='data5.pcd')
    # parser.add_argument('--points', default='data6.pcd')
    args = parser.parse_args()

    # model = PointNet(
    #     n_class=len(pointnet_label_names),
    #     pretrained_model=args.pretrained_model)
    # model = PointNetCls(k = len(pointnet_label_names))
    # num_points
    model = PointNetCls(k = 16)
    # serializers.load_npz('cls_model_4.npz', model)
    serializers.load_npz(args.pretrained_model, model)

    if args.gpu >= 0:
        model.to_gpu(args.gpu)
        chainer.cuda.get_device(args.gpu).use()

    # PyTorch : 2500
    # readDatas
    point = utils.read_points(args.points, color=True)

    # onnx export
    # onnx_chainer.export(model, [point], filename='pointnet_cls.onnx')

    # cls
    print(point)
    # print(point.shape)
    # convert list
    # print([point])
    # print([point][0].shape)
    labels = model.predict([point])
    label = labels[0]

    # matplotlib
    # fig = plt.figure()
    # ax1 = fig.add_subplot(121, projection='3d')
    # vis_pointcloud(point, ax=ax1)
    # ax2 = fig.add_subplot(122, projection='3d')
    # vis_label_3d(label, label_names, label_colors, ax=ax2)
    # vis_label_3d(label, shapenetCorev2_label_names, shapenetCorev2_label_colors, ax=ax1)
    # plt.show()
    # vtk
    # print(point[0].transpose())
    vis_pointcloud_pcl(point[0].transpose())


if __name__ == '__main__':
    main()



# -*- coding: utf-8 -*-
# from __future__ import print_function
import chainer
import chainer.functions as F
import chainer.links as L
from chainer import training
from chainer.training import extensions
import argparse
import os
import random
import numpy as np
from datasets import PartDataset
from modeler import PointNetDenseCls
from chainer import Function, Variable
from chainer import cuda
from chainer import serializers

from chainerpcl.links.model.pointnet import PointNetCls
from chainerpcl.links.model.pointnet import updater
from chainerpcl.datasets import shapenetCorev2_Dataset

def main():
    parser = argparse.ArgumentParser(description='Chainer PointNet PartSegmentation')
    parser.add_argument('--batchsize', type=int, default=32, help='input batch size')
    parser.add_argument('--gpu', '-g', type=int, default=-1, help='GPU ID (negative value indicates CPU)')
    parser.add_argument('--nepoch', type=int, default=25, help='number of epochs to train for')
    parser.add_argument('--epoch', type=int, default=25, help='epoch)')
    parser.add_argument('--outf', type=str, default='seg',  help='output folder')
    parser.add_argument('--model', type=str, default = '',  help='model path')

    opt = parser.parse_args()
    print (opt)

    print('GPU: {}'.format(opt.gpu))
    print('# Minibatch-size: {}'.format(opt.batchsize))
    print('# epoch: {}'.format(opt.epoch))
    print('')

    # Ubuntu?/MacOSX?
    blue = lambda x:'\033[94m' + x + '\033[0m'

    # PyTorch
    # 乱数値設定
    # opt.manualSeed = random.randint(1, 10000) # fix seed
    # print("Random Seed: ", opt.manualSeed)
    # random.seed(opt.manualSeed)
    # # torch.manual_seed(opt.manualSeed)

    # 保存先フォルダの作成
    try:
        os.makedirs(opt.outf)
    except OSError:
        pass

    # PyTorch データ設定
    # dataset = PartDataset(root = 'shapenetcore_partanno_segmentation_benchmark_v0', classification = True, npoints = opt.num_points)
    # dataloader = torch.utils.data.DataLoader(dataset, batch_size=opt.batchSize, shuffle=True, num_workers=int(opt.workers))
    # test_dataset = PartDataset(root = 'shapenetcore_partanno_segmentation_benchmark_v0', classification = True, train = False, npoints = opt.num_points)
    # testdataloader = torch.utils.data.DataLoader(test_dataset, batch_size=opt.batchSize, shuffle=True, num_workers=int(opt.workers))

    # Chainer データ設定
    dataset = PartDataset(root = 'shapenetcore_partanno_segmentation_benchmark_v0' , train = True, classification = False, class_choice = ['Chair'])
    test_dataset = PartDataset(root = 'shapenetcore_partanno_segmentation_benchmark_v0' , train = False, classification = False, class_choice = ['Chair'])

    # ps, seg = dataset[0]
    # print(ps.debug_print())
    # test_ps, test_seg = dataset[0]
    # print(test_ps.debug_print())
    # train_iter = chainer.iterators.SerialIterator(dataset, opt.batchsize, shuffle=True)
    train_iter = chainer.iterators.SerialIterator(dataset, opt.batchsize, repeat=True, shuffle=True)
    test_iter = chainer.iterators.SerialIterator(test_dataset, opt.batchsize, repeat=False, shuffle=True)

    print(len(dataset), len(test_dataset))
    num_classes = dataset.num_seg_classes
    print('classes', num_classes)

    # モデル生成
    # PyTorch
    # classifier = PointNetDenseCls(k = num_classes)
    # Chainer
    classifier = modeler.PointNetDenseCls(k = num_classes)

    # 勾配関数設定(PyTorch)
    # optimizer = optim.SGD(classifier.parameters(), lr=0.01, momentum=0.9)
    # classifier.cuda()
    # 勾配関数設定(Chainer)
    optimizer = chainer.optimizers.MomentumSGD(lr=0.01, momentum=0.9)

    optimizer.setup(classifier)
    # optimizer.zero_grad()

    # Chainer モデル途中再開
    if opt.model != '':
        serializers.load_npz(opt.model, classifier)
        # load(optimizer.setup の後に呼び出す)
        # serializers.load_hdf5(opt.model, classifier)
        # serializers.load_hdf5(opt.optimizer, optimizer)

    if opt.gpu >= 0:
        chainer.cuda.get_device_from_id(opt.gpu).use()
        classifier.to_gpu()

    # use Trainer
    if True:
        # Not use StandardUpdater(NG)
        # updater = training.StandardUpdater(train_iter, optimizer, device=opt.gpu)
        # trainer = training.Trainer(updater, (opt.nepoch, 'epoch'), out=opt.outf)
        # extend(Standard?)
        # trainer.extend(extensions.Evaluator(test_iter, classifier, device=opt.gpu))
        # frequency = opt.epoch if opt.frequency == -1 else max(1, opt.frequency)
        # trainer.extend(extensions.snapshot(), trigger=(frequency, 'epoch'))
        # trainer.extend(extensions.LogReport())
        # trainer.extend(extensions.PrintReport(['epoch', 'main/loss', 'validation/main/loss', 'main/accuracy', 'validation/main/accuracy', 'elapsed_time']))
        # use CustomUpdater
        # カスタム updaterの実装方法/Extensionの自作方法
        # http://mizti.hatenablog.com/entry/2017/09/17/002858
        # http://mizti.hatenablog.com/entry/2017/09/23/202750
        updater_args = {}
        updater_args["iterator"] = train_iter
        updater_args["optimizer"] = optimizer
        updater_args["models"] = classifier
        updater_args["device"] = opt.gpu
        updater2 = updater.PartSegmentaionUpdater(**updater_args)
        trainer = training.Trainer(updater2, (opt.nepoch, 'epoch'), out=opt.outf)
        # extend
        # Set up logging
        report_keys = ["epoch", "iteration", "train_loss", "accuracy",]
        # Save model
        # trainer.extend(extensions.snapshot_object(classifier, '%s/seg_model_%d.npz' % (opt.outf, updater2.epoch)), trigger=(1, 'epoch'))
        # Trainer の out とかぶるため、opt.outf の設定は行わない。
        trainer.extend(extensions.snapshot_object(classifier, 'seg_model_' + '{.updater2.epoch}.npz'), trigger=(1, 'epoch'))
        # save log to file
        trainer.extend(extensions.LogReport(keys=report_keys, log_name="train.log", trigger=(1, 'iteration')))
        # print log
        trainer.extend(extensions.PrintReport(report_keys), trigger=(1, 'iteration'))
        # trainer.extend(extensions.ProgressBar())
        # execute
        trainer.run()
        return

    # no use Trainer(use Iterator)
    # replace xp -> model.xp
    # xp = np if opt.gpu < 0 else cuda.cupy

    num_batch = len(dataset) // opt.batchsize
    print(num_batch)
    # num_batch = num_batch

    i = 0
    while train_iter.epoch < opt.nepoch:
        data = train_iter.next()
        # Convert dataArray
        # PyTorch
        # points, target = data
        # points, target = Variable(points), Variable(target)
        # Chainer
        size = min(opt.batchsize, len(data))
        # points, target = decomp(data, size)
        # def decomp(batch, batchsize):
        x = []
        t = []
        for j in range(size):
            x.append(data[j][0].data)
            t.append(data[j][1].data)
        points, target = Variable(classifier.xp.array(x)), Variable(classifier.xp.array(t))

        # points = points.transpose(2,1) 
        points = points.transpose([0, 2, 1])

        # PyTorch
        # points, target = points.cuda(), target.cuda()
        # optimizer.zero_grad()

        # PyTorch
        # pred, _ = classifier(points)
        # Chainer
        pred, _ = classifier.fwd(points)

        # pred = pred.view(-1, num_classes)
        # target = target.view(-1,1)[:,0] - 1
        pred = pred.reshape(-1, num_classes)
        target = target.reshape(-1,1)[:,0] - 1

        # 80000(2500*32)
        # print(target.debug_print())
        # 80000(2500*32)x4
        # print(pred.debug_print())

        # PyTorch
        # http://pytorch.org/docs/master/nn.html?highlight=nll_loss#torch.nn.functional.nll_loss
        # http://pytorch.org/docs/master/nn.html?highlight=nll_loss#torch.nn.NLLLoss
        # pred : log_softmax function set?
        # loss = F.nll_loss(pred, target)
        # loss = F.nll_loss(F.log_softmax(pred), target)
        # Chainer
        # https://docs.chainer.org/en/stable/reference/generated/chainer.functions.gaussian_nll.html?highlight=likelihood%20loss
        loss = F.softmax_cross_entropy(pred, target)

        # Chainer?
        classifier.cleargrads()

        # print(loss)
        loss.backward()

        # PyTorch
        # optimizer.step()
        # Chainer
        optimizer.update()

        # class check
        # accuracy - PyTorch
        # http://pytorch.org/docs/master/torch.html?highlight=max#torch.max
        # pred_choice = pred.data.max(1)[1]
        # correct = pred_choice.eq(target.data).cpu().sum()
        # numpy - argmax
        # https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.argmax.html
        pred_choice = pred.data.argmax(1)
        correct = pred_choice.__eq__(target.data).sum()
        # print(pred_choice)
        # print(target.data)

        # print(loss)
        # print(loss.data)
        # print(correct)
        print('[%d: %d/%d] train loss: %f accuracy: %f' %(train_iter.epoch, i, num_batch, loss.data, correct/float(size * 2500)))
        i = i + 1


#         if i % 10 == 0:
#             # j, data = enumerate(testdataloader, 0).next()
#             pt2_data = test_iter.__next__()
#             # j, pt2_data = enumerate(test_iter, 0).__next__()
#             # PyTorch
#             # points, target = pt2_data
#             # points, target = Variable(points), Variable(target)
#             size = min(opt.batchsize, len(pt2_data))
#             points, target = decomp(pt2_data, size)
#             # points = points.transpose(2,1) 
#             points = points.transpose([0, 2, 1])
# 
#             # points, target = points.cuda(), target.cuda()
#             # pred, _ = classifier(points)
#             pred, _ = model(points)
# 
#             # PyTorch
#             # pred = pred.view(-1, num_classes)
#             # target = target.view(-1,1)[:,0] - 1
#             pred = pred.reshape(-1, num_classes)
#             target = target.reshape(-1,1)[:,0] - 1
# 
#             # PyTorch
#             # loss = F.nll_loss(pred, target)
#             # NG
#             # loss = F.bernoulli_nll(pred, target, reduce='no')
#             # loss = F.softmax_cross_entropy(pred, target)
#             # OK?-> NG : (loss = nan)
#             loss = F.softmax_cross_entropy(pred, target)
# 
#             # pred_choice = pred.data.max(1)[1]
#             pred_choice = pred.data.argmax(1)
#             # correct = pred_choice.eq(target.data).cpu().sum()
#             correct = pred_choice.__eq__(target.data).sum()
# 
#             print('[%d: %d/%d] %s loss: %f accuracy: %f' %(train_iter.epoch, i, num_batch, blue('test'), loss.data, correct/float(size * 2500)))
#             # print('[%d: %d/%d] %s loss: %f accuracy: %f' %(train_iter.epoch, i, num_batch, blue('test'), loss.data[0], correct/float(size * 2500)))

        if train_iter.is_new_epoch:
            serializers.save_npz('%s/seg_model_%d.npz' % (opt.outf, train_iter.epoch), classifier)
            # serializers.save_hdf5('%s/seg_model_%d.h5' % (opt.outf, train_iter.epoch), classifier)
            # serializers.save_hdf5('%s/seg_optimizer_%d.h5' % (opt.outf, train_iter.epoch), optimizer)
            i = 0


# def decomp(batch, batchsize):
#     x = []
#     t = []
#     for j in range(batchsize):
#         x.append(batch[j][0].data)
#         t.append(batch[j][1].data)
# 
#     return Variable(np.array(x)), Variable(np.array(t))


if __name__ == '__main__':
    main()

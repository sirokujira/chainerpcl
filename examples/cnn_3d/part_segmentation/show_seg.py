from __future__ import print_function
from show3d_balls import *
import argparse
import os
import random
import numpy as np

import modeler
import datasets

import chainer
import chainer.functions as F
import chainer.links as L
from chainer import training
from chainer.training import extensions

from datasets import PartDataset
from pointnet import PointNetDenseCls
from chainer import Function, Variable
from chainer import serializers
from chainer import cuda

import matplotlib.pyplot as plt

def main():
    #showpoints(np.random.randn(2500,3), c1 = np.random.uniform(0,1,size = (2500)))

    parser = argparse.ArgumentParser()

    parser.add_argument('--model', type=str, default = '',  help='model path')
    parser.add_argument('--idx', type=int, default = 0,   help='model index')
    parser.add_argument('--gpu', '-g', type=int, default=-1, help='GPU ID (negative value indicates CPU)')

    opt = parser.parse_args()
    print (opt)

    d = PartDataset(root = 'shapenetcore_partanno_segmentation_benchmark_v0', class_choice = ['Chair'], train = False)

    idx = opt.idx

    print("model %d/%d" %( idx, len(d)))

    point, seg = d[idx]
    # print(point.size(), seg.size())
    print(point.debug_print(), seg.debug_print())

    # point_np = point.numpy()
    point_np = point.data

    cmap = plt.cm.get_cmap("hsv", 10)
    cmap = np.array([cmap(i) for i in range(10)])[:,:3]
    # gt = cmap[seg.numpy() - 1, :]
    gt = cmap[seg.data - 1, :]

    classifier = PointNetDenseCls(k = 4)
    serializers.load_npz(opt.model, classifier)

    # point = point.transpose(1,0).contiguous()
    point = point.transpose(1,0)

    # point = Variable(point.view(1, point.size()[0], point.size()[1]))
    # print(point.debug_print())
    # print(point.size()[1])
    # point = Variable(point.reshape(1, point.size()[0], point.size()[1]))
    point = point.reshape(1, 3, 2500)
    pred, _ = classifier.fwd(point)

    print(pred.debug_print())

    # PyTorch
    # pred_choice = pred.data.max(2)[1][0,:,0]
    # print(pred.data.max(2))
    pred_choice = pred.data.argmax(2)
    # 2500
    # print(pred_choice.size())
    print(pred_choice)
    # pred_color = cmap[pred_choice.numpy(), :]
    pred_color = cmap[pred_choice, :]

    #print(pred_color.shape)

    showpoints(point_np, gt, pred_color)

if __name__ == '__main__':
    main()

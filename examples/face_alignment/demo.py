import argparse
# https://matplotlib.org/examples/mplot3d/scatter3d_demo.html
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import chainer

from chainerpcl.datasets import shapenet
from chainerpcl.links.model.face_alignment import FaceAlignment
from chainerpcl import utils
from chainerpcl.visualizations import vis_pointcloud, vis_pointcloud_pcl
from chainerpcl.visualizations import vis_label_3d
from chainer import serializers

import numpy as np
import onnx_chainer

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from skimage import io

shapenetCorev2_label_names = (
    'Airplane',
    'Bag',
    'Cap',
    'Car',
    'Chair',
    'Earphone',
    'Guitar',
    'Knife',
    'Lamp',
    'Laptop',
    'Motorbike',
    'Mug',
    'Pistol',
    'Rocket',
    'Skateboard',
    'Table'
)

shapenetCorev2_label_colors = (
    (128, 128, 128),
    (128, 0, 0),
    (192, 192, 128),
    (128, 64, 128),
    (60, 40, 222),
    (128, 128, 0),
    (192, 128, 128),
    (64, 64, 128),
    (64, 0, 128),
    (64, 64, 0),
    (0, 128, 192),
    (0, 32, 0),
    (0, 0, 32),
    (0, 96, 0),
    (0, 160, 96),
    (0, 160, 160)
)


def main():
    chainer.config.train = False

    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', type=int, default=-1)
    parser.add_argument('--pretrained_model', default='face_model_4.npz')
    # parser.add_argument('--pointnum', default=2500)
    parser.add_argument('--points', default='data1.pcd')
    args = parser.parse_args()

    # model = PointNet(
    #     n_class=len(pointnet_label_names),
    #     pretrained_model=args.pretrained_model)
    # model = PointNetCls(k = len(pointnet_label_names))
    # num_points
    model = PointNetCls(k = 16)
    # serializers.load_npz('cls_model_4.npz', model)
    serializers.load_npz(args.pretrained_model, model)

    # Run the 3D face alignment on a test image, without CUDA.
    # model = FaceAlignment(face_alignment.LandmarksType._3D, enable_cuda=False, flip_input=False)
    # input = io.imread('../test/assets/aflw-test.jpg')
    # preds = model.get_landmarks(input)[-1]

    if args.gpu >= 0:
        model.to_gpu(args.gpu)
        chainer.cuda.get_device(args.gpu).use()

    # PyTorch : 2500
    # readDatas
    # point = utils.read_points(args.points, color=True)
    # onnx export
    # onnx_chainer.export(model, [point], filename='pointnet_cls.onnx')

    # cls
    # print(point)
    # print(point.shape)
    # convert list
    # print([point])
    # print([point][0].shape)
    labels = model.predict([point])
    label = labels[0]

    # matplotlib
    #TODO: Make this nice
    fig = plt.figure(figsize=plt.figaspect(.5))
    ax = fig.add_subplot(1, 2, 1)
    ax.imshow(input)
    ax.plot(preds[0:17,0],preds[0:17,1],marker='o',markersize=6,linestyle='-',color='w',lw=2)
    ax.plot(preds[17:22,0],preds[17:22,1],marker='o',markersize=6,linestyle='-',color='w',lw=2)
    ax.plot(preds[22:27,0],preds[22:27,1],marker='o',markersize=6,linestyle='-',color='w',lw=2)
    ax.plot(preds[27:31,0],preds[27:31,1],marker='o',markersize=6,linestyle='-',color='w',lw=2)
    ax.plot(preds[31:36,0],preds[31:36,1],marker='o',markersize=6,linestyle='-',color='w',lw=2)
    ax.plot(preds[36:42,0],preds[36:42,1],marker='o',markersize=6,linestyle='-',color='w',lw=2)
    ax.plot(preds[42:48,0],preds[42:48,1],marker='o',markersize=6,linestyle='-',color='w',lw=2)
    ax.plot(preds[48:60,0],preds[48:60,1],marker='o',markersize=6,linestyle='-',color='w',lw=2)
    ax.plot(preds[60:68,0],preds[60:68,1],marker='o',markersize=6,linestyle='-',color='w',lw=2) 
    ax.axis('off')

    ax = fig.add_subplot(1, 2, 2, projection='3d')
    surf = ax.scatter(preds[:,0]*1.2,preds[:,1],preds[:,2],c="cyan", alpha=1.0, edgecolor='b')
    ax.plot3D(preds[:17,0]*1.2,preds[:17,1], preds[:17,2], color='blue' )
    ax.plot3D(preds[17:22,0]*1.2,preds[17:22,1],preds[17:22,2], color='blue')
    ax.plot3D(preds[22:27,0]*1.2,preds[22:27,1],preds[22:27,2], color='blue')
    ax.plot3D(preds[27:31,0]*1.2,preds[27:31,1],preds[27:31,2], color='blue')
    ax.plot3D(preds[31:36,0]*1.2,preds[31:36,1],preds[31:36,2], color='blue')
    ax.plot3D(preds[36:42,0]*1.2,preds[36:42,1],preds[36:42,2], color='blue')
    ax.plot3D(preds[42:48,0]*1.2,preds[42:48,1],preds[42:48,2], color='blue')
    ax.plot3D(preds[48:,0]*1.2,preds[48:,1],preds[48:,2], color='blue' )

    ax.view_init(elev=90., azim=90.)
    ax.set_xlim(ax.get_xlim()[::-1])
    plt.show()
    # pointcloudlibrary
    # vis_pointcloud_pcl(point[0].transpose())


if __name__ == '__main__':
    main()



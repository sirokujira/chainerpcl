3DGAN
========

# Preparation

Please create `class_weight.npy` using calc_weight.py first. Just run:

```
python calc_weight.py
```

# Start training

First, move to this directory (i.e., `examples/3DGAN`) and run:

```
python train.py [--gpu <gpu>]
```

## Experimental settings

We used the completely same parameters for all settings.

| Implementation | Optimizer   | Learning rage | Momentum | Weight decay | Model code |
|:--------------:|:-----------:|:-------------:|:--------:|:------------:|:----------:|
| chainerpcl     | adam        | 0.1           | 0.9      | 0.001        | [3DGAN_basic.py](https://github.com/chainer/chainerpcl/tree/master/chainerpcl/links/model/3DGAN/3DGAN_basic.py) |
| Official       | adam        | 0.001         | 0.9      | 0.001        | [3DGAN_cls.py](https://github.com/charlesq34/3DGAN/blob/master/models/3DGAN_cls.py) |

# Quick Demo

Here is a quick demo using our pretrained weights. The pretrained model is automatically downloaded from the internet.

```
wget https://raw.githubusercontent.com/alexgkendall/3DGAN-Tutorial/master/3DGAN/test/0001TP_008550.png
python demo.py [--gpu <gpu>] [--pretrained_model <model_path>] 0001TP_008550.png
```


# Evaluation

The trained weights to replicate the same results as below is here: [model_iteration-16000](https://www.dropbox.com/s/exas66necaqbxyw/model_iteration-16000).

```
python eval_3DGAN.py [--gpu <gpu>] [--pretrained_model <model_path>] [--batchsize <batchsize>]
```


# Results

Once you execute the above evaluation script, you will see the values of intersection over union (IoU) for each class, the mean IoU over all the classes, class average accuracy, and global average accuracy like this:

```
                    Sky : 0.8790
               Building : 0.6684
                   Pole : 0.1923
                   Road : 0.8739
               Pavement : 0.6421
                   Tree : 0.6227
             SignSymbol : 0.1893
                  Fence : 0.2137
                    Car : 0.6355
             Pedestrian : 0.2739
              Bicyclist : 0.2415
==================================
               mean IoU : 0.4939
 Class average accuracy : 0.6705
Global average accuracy : 0.8266
```

## Comparizon with the paper results

| Implementation | Global accuracy | Class accuracy | mean IoU   |
|:--------------:|:---------------:|:--------------:|:----------:|
| chainerpcl     | ??.? %          | **??.? %**     | **??.? %** |
| Official       | **82.8 %**      | 62.3%          | ??.? %     |

The above values of the official implementation is found here: [Getting Started with 3DGAN](http://mi.eng.cam.ac.uk/projects/3DGAN/tutorial.html)

# Reference

1. Author- "3DGAN: A Deep Convolutional Encoder-Decoder Architecture for Image Segmentation." PAMI, 2017. 
2. Vijay Badrinarayanan, Ankur Handa and Roberto Cipolla "3DGAN: A Deep Convolutional Encoder-Decoder Architecture for Robust Semantic Pixel-Wise Labelling." arXiv preprint arXiv:1505.07293, 2015.

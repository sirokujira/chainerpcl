import argparse
import matplotlib.pyplot as plt

import chainer

from chainerpcl.datasets import pointnet_label_colors
from chainerpcl.datasets import pointnet_label_names
from chainerpcl.links import PointNetCLS
from chainerpcl import utils
# from chainerpcl.visualizations import vis_img
from chainerpcl.visualizations import vis_pointcloud
from chainerpcl.visualizations import vis_label


def main():
    chainer.config.train = False

    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', type=int, default=-1)
    parser.add_argument('--pretrained_model', default='3dgan')
    parser.add_argument('points')
    args = parser.parse_args()

    model = PointNetCLS(
        n_class=len(pointnet_label_names),
        pretrained_model=args.pretrained_model)

    if args.gpu >= 0:
        model.to_gpu(args.gpu)
        chainer.cuda.get_device(args.gpu).use()

    points = utils.read_point(args.points, color=True)
    labels = model.predict([points])
    label = labels[0]

    fig = plot.figure()
    ax1 = fig.add_subplot(1, 2, 1)
    vis_voxelcloud(points, ax=ax1)
    ax2 = fig.add_subplot(1, 2, 2)
    vis_voxelcloud(label, pointnet_label_names, pointnet_label_colors, ax=ax2)
    plot.show()


if __name__ == '__main__':
    main()

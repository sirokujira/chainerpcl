import argparse
# https://matplotlib.org/examples/mplot3d/scatter3d_demo.html
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import chainer

from chainerpcl.datasets import shapenet
from chainerpcl.links.model.pointnet import PointNetDenseCls
from chainerpcl import utils
from chainerpcl.visualizations import vis_pointcloud, vis_pointcloud_pcl, vis_pointcloud_pcl2
from chainerpcl.visualizations import vis_label_3d
from chainer import serializers

import numpy as np
# import onnx_chainer

# Chair
ShapeNetCorev2_partsegmentation_names = (
    '1',
    '2',
    '3',
    '4',
)

# ShapeNetCorev2_partsegmentation_colors = (
#     (128, 128, 128),
#     (128, 0, 0),
#     (192, 192, 128),
#     (128, 64, 128),
# )
from collections import namedtuple
Color = namedtuple("Color", "B G R")
ShapeNetCorev2_partsegmentation_colors = (
    128 * pow(2, 16) + 128 * pow(2, 8) + 128,
    128 * pow(2, 16) + 255 * pow(2, 8) + 0,
    192 * pow(2, 16) + 192 * pow(2, 8) + 128,
    128 * pow(2, 16) + 64  * pow(2, 8) + 128,
)


def main():
    chainer.config.train = False

    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', type=int, default=-1)
    parser.add_argument('--pretrained_model', default='seg_model_7.npz')
    # parser.add_argument('--pointnum', default=2500)
    # parser.add_argument('--points', default='bunny.pcd')
    # parser.add_argument('--points', default='ism_train_cat.pcd')
    # parser.add_argument('--points', default='model_000056.obj')
    parser.add_argument('--points', default='data1.pcd')
    # parser.add_argument('--points', default='data2.pcd')
    # parser.add_argument('--points', default='data3.pcd')
    # parser.add_argument('--points', default='data4.pcd')
    # parser.add_argument('--points', default='data5.pcd')
    # parser.add_argument('--points', default='data6.pcd')
    args = parser.parse_args()

    model = PointNetDenseCls(k = 4)
    serializers.load_npz(args.pretrained_model, model)

    if args.gpu >= 0:
        model.to_gpu(args.gpu)
        chainer.cuda.get_device(args.gpu).use()

    # PyTorch : 2500
    # readDatas
    point = utils.read_points(args.points, color=True)
    point = point.reshape(1, 3, 2500)
    pred, _ = model.fwd(point)

    print(pred.debug_print())
    pred_choices = pred.data.argmax(2)
    print(pred_choices)
    print(pred_choices[0])
    # pred_color = cmap[pred_choices.numpy(), :]
    # pred_colors = ShapeNetCorev2_partsegmentation_colors[0]
    pred_colors = []
    for pred_choice in pred_choices[0]:
        # print(pred_choice)
        part_color = ShapeNetCorev2_partsegmentation_colors[pred_choice]
        pred_colors.append(part_color)

    # print(pred_colors)

    # onnx export
    # onnx_chainer.export(model, [point], filename='pointnet_cls.onnx')

    # labels = model.predict([point])
    # label = labels[0]

    # matplotlib
    # fig = plt.figure()
    # ax1 = fig.add_subplot(121, projection='3d')
    # vis_pointcloud(point, ax=ax1)
    # ax2 = fig.add_subplot(122, projection='3d')
    # vis_label_3d(label, label_names, label_colors, ax=ax2)
    # vis_label_3d(label, ShapeNetCorev2_label_names, ShapeNetCorev2_seg_label_colors, ax=ax1)
    # plt.show()
    # vtk
    # print(point[0].transpose())
    vis_pointcloud_pcl2(point[0].transpose(), pred_colors)


if __name__ == '__main__':
    main()



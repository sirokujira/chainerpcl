PointNet
========

# Start training

First, move to this directory (i.e., `examples/PointNet`) and run:

```
python classification/train_classification.py [--gpu <gpu>]
python part_segmentation/train_segmentation.py [--gpu <gpu>]
# python semantic_segmentation/train_segmentation.py [--gpu <gpu>]
```

## Experimental settings

We used the completely same parameters for all settings.

| Implementation        | Optimizer   | Learning rage | Momentum | Weight decay | Model code |
|:---------------------:|:-----------:|:-------------:|:--------:|:------------:|:----------:|
| Official(Tensorflow)  | adam        | 0.001         | 0.9      | 0.001        | [PointNet_cls.py](https://github.com/charlesq34/pointnet/blob/master/models/pointnet_cls.py) |
| PyTorch               | adam        | 0.001         | 0.9      | 0.001        | [pointnet.py](https://github.com/fxia22/pointnet.pytorch/blob/master/pointnet.py) |
| chainerpcl            | adam        | 0.001         | 0.9      | 0.001        | [PointNet.py](https://github.com/sirokujira/chainerpcl/tree/master/chainerpcl/links/model/PointNet/PointNet.py) |

# Quick Demo

Here is a quick demo using our pretrained weights. The pretrained model is automatically downloaded from the internet.

```
wget bunny?.pcd
python classification/demo.py [--gpu <gpu>] [--pretrained_model <model_path>] bunny?.pcd
python part_segmentation/demo.py [--gpu <gpu>] [--pretrained_model <model_path>] bunny?.pcd
python semantic_segmentation/demo.py [--gpu <gpu>] [--pretrained_model <model_path>] bunny?.pcd
```


# Evaluation

The trained weights to replicate the same results as below is here: [model_iteration-16000](https://www.dropbox.com/s/exas66necaqbxyw/model_iteration-16000).

```
python eval_pointnet.py [--gpu <gpu>] [--pretrained_model <model_path>] [--batchsize <batchsize>]
```


# Results

Once you execute the above evaluation script, you will see the values of intersection over union (IoU) for each class, the mean IoU over all the classes, class average accuracy, and global average accuracy like this:

```
                Airplane : 0.????
                     Bag : 0.????
                     Cap : 0.????
                     Car : 0.????
                   Chair : 0.????
                Earphone : 0.????
                  Guitar : 0.????
                   Knife : 0.????
                    Lamp : 0.????
                  Laptop : 0.????
               Motorbike : 0.????
                     Mug : 0.????
                  Pistol : 0.????
                  Rocket : 0.????
              Skateboard : 0.????
                   Table : 0.????
==================================
               mean IoU? : 0.????
 Class average accuracy? : 0.????
Global average accuracy? : 0.????
```

## Comparizon with the paper results

| Implementation | Global accuracy | Class accuracy | mean IoU   |
|:--------------:|:---------------:|:--------------:|:----------:|
| Official       | **??.? %**      | ??.?%          | ??.? %     |
| PyTorch        | ??.? %          | **??.? %**     | **??.? %** |
| chainerpcl     | ??.? %          | **??.? %**     | **??.? %** |

# base?
# http://modelnet.cs.princeton.edu/

The above values of the official implementation is found here: [Getting Started with PointNet](http://mi.eng.cam.ac.uk/projects/PointNet/tutorial.html)

# Reference

1. Author- "PointNet: A Deep Convolutional Encoder-Decoder Architecture for Image Segmentation." PAMI, 2017. 
2. Vijay Badrinarayanan, Ankur Handa and Roberto Cipolla "PointNet: A Deep Convolutional Encoder-Decoder Architecture for Robust Semantic Pixel-Wise Labelling." arXiv preprint arXiv:1505.07293, 2015.

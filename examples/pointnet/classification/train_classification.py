# -*- coding: utf-8 -*-
import chainer
import chainer.functions as F
import chainer.links as L
from chainer import training
from chainer.training import extensions
import argparse
import os
import random
import numpy as np
from chainer import Function, Variable
from chainer import cuda
from chainer import serializers

from chainerpcl.links.model.pointnet import PointNetCls
from chainerpcl.links.model.pointnet import updater
from chainerpcl.datasets.shapenet.shapenetcorev2_dataset import ShapeNetCorev2Dataset

# Avoid ssl check
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

def main():
    parser = argparse.ArgumentParser(description='Chainer PointNet Classification')
    parser.add_argument('--batchsize', type=int, default=32, help='input batch size')
    parser.add_argument('--num_points', type=int, default=2500, help='input batch size')
    parser.add_argument('--gpu', '-g', type=int, default=-1, help='GPU ID (negative value indicates CPU)')
    parser.add_argument('--nepoch', type=int, default=25, help='number of epochs to train for')
    parser.add_argument('--epoch', type=int, default=25, help='epoch)')
    parser.add_argument('--outf', type=str, default='cls',  help='output folder')
    parser.add_argument('--model', type=str, default = '',  help='model path')

    opt = parser.parse_args()
    print (opt)

    print('GPU: {}'.format(opt.gpu))
    print('# Minibatch-size: {}'.format(opt.batchsize))
    print('# epoch: {}'.format(opt.epoch))
    print('')

    # Ubuntu?/MacOSX?
    blue = lambda x:'\033[94m' + x + '\033[0m'

    # PyTorch
    # 乱数値設定
    # opt.manualSeed = random.randint(1, 10000) # fix seed
    # print("Random Seed: ", opt.manualSeed)
    # random.seed(opt.manualSeed)
    # # torch.manual_seed(opt.manualSeed)

    # 保存先フォルダの作成
    try:
        os.makedirs(opt.outf)
    except OSError:
        pass

    # PyTorch データ設定
    # dataset = PartDataset(root = 'shapenetcore_partanno_segmentation_benchmark_v0', classification = True, npoints = opt.num_points)
    # dataloader = torch.utils.data.DataLoader(dataset, batch_size=opt.batchSize, shuffle=True, num_workers=int(opt.workers))
    # test_dataset = PartDataset(root = 'shapenetcore_partanno_segmentation_benchmark_v0', classification = True, train = False, npoints = opt.num_points)
    # testdataloader = torch.utils.data.DataLoader(test_dataset, batch_size=opt.batchSize, shuffle=True, num_workers=int(opt.workers))

    # Chainer データ設定
    dataset = ShapeNetCorev2Dataset(root = 'shapenetcore_partanno_segmentation_benchmark_v0' , train = True, classification = True, npoints = opt.num_points)
    test_dataset = ShapeNetCorev2Dataset(root = 'shapenetcore_partanno_segmentation_benchmark_v0' , train = False, classification = True, npoints = opt.num_points)

    # ps, cls = dataset[0]
    # print(ps.debug_print())
    # test_ps, test_cls = test_dataset[0]
    # print(test_ps.debug_print())
    # train_iter = chainer.iterators.SerialIterator(dataset, opt.batchsize, shuffle=True)
    train_iter = chainer.iterators.SerialIterator(dataset, opt.batchsize, repeat=True, shuffle=True)
    test_iter = chainer.iterators.SerialIterator(test_dataset, opt.batchsize, repeat=False, shuffle=True)

    print(len(dataset), len(test_dataset))
    num_classes = len(dataset.classes)
    print('classes', num_classes)

    # モデル生成
    classifier = PointNetCls(k = num_classes, num_points = opt.num_points)

    # 勾配関数設定(PyTorch)
    # optimizer = optim.SGD(classifier.parameters(), lr=0.01, momentum=0.9)
    # classifier.cuda()
    # 勾配関数設定(Chainer)
    optimizer = chainer.optimizers.MomentumSGD(lr=0.01, momentum=0.9)

    optimizer.setup(classifier)
    # optimizer.zero_grad()

    # Chainer モデル途中再開
    if opt.model != '':
        serializers.load_npz(opt.model, classifier)
        # load(optimizer.setup の後に呼び出す)
        # serializers.load_hdf5(opt.model, classifier)
        # serializers.load_hdf5(opt.optimizer, optimizer)

    if opt.gpu >= 0:
       chainer.cuda.get_device_from_id(opt.gpu).use()
       classifier.to_gpu()

    # use Trainer
    if True:
        # Not use StandardUpdater(NG)
        # updater = training.StandardUpdater(train_iter, optimizer, device=opt.gpu)
        # trainer = training.Trainer(updater, (opt.nepoch, 'epoch'), out=opt.outf)
        # extend(Standard?)
        # trainer.extend(extensions.Evaluator(test_iter, classifier, device=opt.gpu))
        # frequency = opt.epoch if opt.frequency == -1 else max(1, opt.frequency)
        # trainer.extend(extensions.snapshot(), trigger=(frequency, 'epoch'))
        # trainer.extend(extensions.LogReport())
        # trainer.extend(extensions.PrintReport(['epoch', 'main/loss', 'validation/main/loss', 'main/accuracy', 'validation/main/accuracy', 'elapsed_time']))
        # use CustomUpdater
        # Extension リスト
        # http://mizti.hatenablog.com/entry/2017/09/23/202750
        updater_args = {}
        updater_args["iterator"] = train_iter
        updater_args["optimizer"] = optimizer
        updater_args["models"] = classifier
        updater_args["device"] = opt.gpu
        updater2 = updater.ClassificationUpdater(**updater_args)
        trainer = training.Trainer(updater2, (opt.nepoch, 'epoch'), out=opt.outf)
        # extend
        # Set up logging
        report_keys = ["epoch", "iteration", "train_loss", "accuracy",]
        # Save model
        # trainer.extend(extensions.snapshot_object(classifier, '%s/cls_model_%d.npz' % (opt.outf, updater2.epoch)), trigger=(1, 'epoch'))
        # Trainer の out とかぶるため、opt.outf の設定は行わない。
        trainer.extend(extensions.snapshot_object(classifier, 'cls_model_' + '{.updater.epoch}.npz'), trigger=(1, 'epoch'))
        # save log to file
        trainer.extend(extensions.LogReport(keys=report_keys, log_name="train.log", trigger=(1, 'iteration')))
        # print log
        trainer.extend(extensions.PrintReport(report_keys), trigger=(1, 'iteration'))
        # trainer.extend(extensions.ProgressBar())
        # execute
        trainer.run()
        return

    # no use Trainer(use Iterator)
    # xp = np if opt.gpu < 0 else cuda.cupy
    xp = classifier.xp

    num_batch = len(dataset) // opt.batchsize
    # print(num_batch)
    num_batch = num_batch - 1

    i = 0
    while train_iter.epoch < opt.nepoch:
        data = train_iter.next()
        # Convert dataArray
        # PyTorch
        # points, target = data[0]
        # points, target = Variable(points), Variable(target[:,0])
        # NG?(convert numpy data)
        # Chainer
        size = min(opt.batchsize, len(data))
        # points, target = decomp(data, size)
        # def decomp(batch, batchsize):
        x = []
        t = []
        for j in range(size):
            x.append(data[j][0].data)
            t.append(data[j][1].data)
        points, target = Variable(xp.array(x)), Variable(xp.array(t))

        # move
        # print(target)
        target = Variable(target.data[:,0])
        # print(target)
        # PyTorch
        # optimizer.zero_grad()

        # 32x2500x3
        # print(points)
        # print(points.debug_print())
        # 32x1
        # print(target)
        # print(target.debug_print())
        # Convert 2dim -> 1dim
        # print('dim - 2')
        # print(target.debug_print())
        # NG?
        # target = Variable(target.data[:,0])
        # target = F.reshape(target, (32,))
        # print('dim - 1')
        # print(target.debug_print())
        points = points.transpose([0, 2, 1])

        # PyTorch
        # http://pytorch.org/docs/master/nn.html?highlight=nll_loss#torch.nn.functional.nll_loss
        # http://pytorch.org/docs/master/nn.html?highlight=nll_loss#torch.nn.NLLLoss
        # pred : log_softmax function set?
        # pred, _ = classifier(points)
        # loss = F.nll_loss(pred, target)
        # loss(x, class) = -x[class]
        # loss = F.nll_loss(F.log_softmax(pred), target)
        # Chainer
        # pred, _ = classifier(points)
        # https://docs.chainer.org/en/stable/reference/generated/chainer.functions.gaussian_nll.html?highlight=likelihood%20loss
        # loss = F.softmax_cross_entropy(pred, target)
        # loss = classifier(points, target)
        pred, _ = classifier.fwd(points)
        loss = F.softmax_cross_entropy(pred, target)

        # Chainer?
        classifier.cleargrads()

        # print(loss)
        loss.backward()

        # PyTorch
        # optimizer.step()
        # Chainer
        optimizer.update()

        # class check
        # accuracy - PyTorch
        # http://pytorch.org/docs/master/torch.html?highlight=max#torch.max
        # pred_choice = pred.data.max(1)[1]
        # correct = pred_choice.eq(target.data).cpu().sum()
        # numpy - argmax
        # https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.argmax.html
        pred_choice = pred.data.argmax(1)
        correct = pred_choice.__eq__(target.data).sum()
        # print(pred_choice)
        # print(target.data)

        # print(loss)
        # print(loss.data)
        # print(correct)
        print('[%d: %d/%d] train loss: %f accuracy: %f' %(train_iter.epoch, i, num_batch, loss.data, correct/float(size)))
        i = i + 1


#         if i % 10 == 0:
#             # j, data = enumerate(testdataloader, 0).next()
#             pt2_data = test_iter.__next__()
# 
#             # points, target = pt2_data
#             # points, target = Variable(points), Variable(target[:,0])
#             size = min(opt.batchsize, len(data))
#             points, target = decomp(pt2_data, size)
#             # Convert 2dim -> 1dim
#             target = Variable(target.data[:,0])
# 
#             points = points.transpose([0, 2, 1])
#             # points, target = points.cuda(), target.cuda()
#             # pred, _ = classifier(points)
#             pred, _ = classifier(points)
# 
#             # PyTorch
#             # loss = F.nll_loss(pred, target)
#             # NG
#             # loss = F.bernoulli_nll(pred, target, reduce='no')
#             # OK?-> NG : (loss = nan)
#             loss = F.softmax_cross_entropy(pred, target)
#             # print(loss)
#             # loss = Variable(loss)
# 
#             # pred_choice = pred.data.max(1)[1]
#             pred_choice = pred.data.argmax(1)
#             # correct = pred_choice.eq(target.data).cpu().sum()
#             correct = pred_choice.__eq__(target.data).sum()
# 
#             print('[%d: %d/%d] %s loss: %f accuracy: %f' %(train_iter.epoch, i, num_batch, blue('test'), loss.data, correct/float(size)))

        if train_iter.is_new_epoch:
            serializers.save_npz('%s/cls_model_%d.npz' % (opt.outf, train_iter.epoch), classifier)
            # serializers.save_hdf5('%s/cls_model_%d.h5' % (opt.outf, train_iter.epoch), classifier)
            # serializers.save_hdf5('%s/cls_optimizer_%d.h5' % (opt.outf, train_iter.epoch), optimizer)
            i = 0


# def decomp(batch, batchsize):
#     x = []
#     t = []
#     for j in range(batchsize):
#         x.append(batch[j][0].data)
#         t.append(batch[j][1].data)
# 
#     return Variable(np.array(x)), Variable(np.array(t))


if __name__ == '__main__':
    main()

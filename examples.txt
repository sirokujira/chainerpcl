### pointnet ###
# demo(classification)
python examples\pointnet\classification\demo.py
python examples\pointnet\classification\demo.py --points teapot.obj
python examples\pointnet\classification\demo.py --pretrained_model cls_model_4.npz
# demo(part_segmentation)
python examples\pointnet\part_segmentation\demo.py
python examples\pointnet\part_segmentation\demo.py --points teapot.obj
python examples\pointnet\part_segmentation\demo.py --pretrained_model seg_model_7.npz

# Train
python examples\pointnet\classification\train_classification.py
python examples\pointnet\classification\train_classification.py --gpu=0

python examples\pointnet\part_segmentation\train_segmentation.py
python examples\pointnet\part_segmentation\train_segmentation.py --gpu=0

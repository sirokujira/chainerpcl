Extensions
==========

.. module:: ChainerPCL.extensions


Evaluator
---------

DetectionVOCEvaluator
~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: DetectionVOCEvaluator

SemanticSegmentationEvaluator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: SemanticSegmentationEvaluator


Visualization Report
--------------------

DetectionVisReport
~~~~~~~~~~~~~~~~~~
.. autoclass:: DetectionVisReport

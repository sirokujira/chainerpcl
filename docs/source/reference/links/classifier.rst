Classifier
==========

.. module:: ChainerPCL.links

PixelwiseSoftmaxClassifier
--------------------------
.. autoclass:: PixelwiseSoftmaxClassifier
   :members:
   :special-members:  __call__

SegNet
======

.. module:: ChainerPCL.links.model.segnet


Semantic Segmentation Link
--------------------------

SegNetBasic
~~~~~~~~~~~
.. autoclass:: SegNetBasic
   :members:
   :special-members:  __call__

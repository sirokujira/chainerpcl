=========
ChainerPCL
=========


ChainerPCL is a **deep learning based computer vision library** built on top of `Chainer <https://github.com/sirokujira/chainer/>`_. 


Install Guide
=============

Pip
~~~

You can install ChainerPCL using `pip`.

.. code-block:: shell

    # If Cython has not been installed yet, install it by a command like
    # pip install Cython
    pip install ChainerPCL


Anaconda
~~~~~~~~

You can setup ChainerPCL including its dependencies using anaconda.

.. code-block:: shell

    # For python 3
    # wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh;
    wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh -O miniconda.sh

    bash miniconda.sh -b -p $HOME/miniconda
    export PATH="$HOME/miniconda/bin:$PATH"
    conda config --set always_yes yes --set changeps1 no
    conda update -q conda
    
    # Download ChainerPCL and go to the root directory of ChainerPCL
    git clone https://github.com/sirokujira/ChainerPCL
    cd ChainerPCL
    conda env create -f environment.yml
    source activate ChainerPCL

    # Install ChainerPCL
    pip install -e .

    # Try our demos at examples/* !


Reference Manual
================

.. toctree::
   :maxdepth: 1

   install
   tutorial/index
   reference/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

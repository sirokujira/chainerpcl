# -*- coding: utf-8 -*-
import chainer
import chainer.functions as F
import chainer.links as L
import numpy as np
import cupy
import numpy.random
from chainer import Function, Variable

from chainer import cuda

from chainerpcl.utils import download_model


class VoxelNet(chainer.Chain):
    def __init__(self, num_points = 2500, k = 1, pad=0):
        super(STN3d, self).__init__(
            # Block1
            conv1 = L.Convolution2D(in_channels=128, out_channels=128, ksize=[3, 2, 1] * 1, pad=0, nobias=False),
            conv2 = L.Convolution2D(in_channels=128, out_channels=128, ksize=[3, 1, 1] * 3, pad=0, nobias=False),
            # Block2
            conv3 = L.Convolution2D(in_channels=128, out_channels=128, ksize=[3, 2, 1] * 1, pad=0, nobias=False),
            conv4 = L.Convolution2D(in_channels=128, out_channels=128, ksize=[3, 1, 1] * 5, pad=0, nobias=False),
            # Block3
            conv5 = L.Convolution2D(in_channels=128, out_channels=256, ksize=[3, 2, 1] * 1, pad=0, nobias=False),
            conv6 = L.Convolution2D(in_channels=128, out_channels=256, ksize=[3, 1, 1] * 5, pad=0, nobias=False),

            fc1 = L.Linear(1024, 512),
            fc2 = L.Linear(512, 256),
            fc3 = L.Linear(256, 9),

            bn1 = L.BatchNormalization(64),
            bn2 = L.BatchNormalization(128),
            bn3 = L.BatchNormalization(1024),
            bn4 = L.BatchNormalization(512),
            bn5 = L.BatchNormalization(256),
        )
        self.num_points = num_points


    def __call__(self, x, y):
        x = self.fwd(x)
        # Probability score map
        # x = 
        # Regression map
        # t = 
        return F.softmax_cross_entropy(x, y)


    def fwd(self, x):
        batchsize = len(x.data[:])
        # Block1
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        # Block2
        x = F.relu(self.bn3(self.conv3(x)))
        x = F.relu(self.bn3(self.conv3(x)))
        # Block3
        x = F.relu(self.bn3(self.conv3(x)))
        x = F.relu(self.bn3(self.conv3(x)))

        # cat
        x = F.concat(a, b, c)
        return x


if __name__ == '__main__':
    sim_data = numpy.random.randn(32, 3, 2500)
    sim_data = self.xp.array(sim_data, dtype=self.xp.float32)
    sim_data = Variable(sim_data)
    print(sim_data.debug_print())
    # print(sim_data)
    # print(sim_data[0])
    trans = STN3d()
    out = trans.fwd(sim_data)
    # 32x3x3
    print('stn\n')
    print(out.debug_print())

    pointfeat = PointNetfeat(global_feat=True)
    out, _ = pointfeat.fwd(sim_data)
    # print('global feat', out.size())
    # 32x1024
    print('global feat(True)\n')
    print(out.debug_print())

    pointfeat = PointNetfeat(global_feat=False)
    out, _ = pointfeat.fwd(sim_data)
    # print('point feat', out.size())
    # 32x1088x2500
    print('point feat(False)\n')
    print(out.debug_print())

    cls = PointNetCls(k = 5)
    out, _ = cls.fwd(sim_data)
    # print('class', out.size())
    # 32x5
    print('class\n')
    print(out.debug_print())

    seg = PointNetDenseCls(k = 3)
    out, _ = seg.fwd(sim_data)
    # print('seg', out.size())
    # 32x2500x3
    print('seg\n')
    print(out.debug_print())



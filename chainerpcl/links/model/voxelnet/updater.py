import numpy as np

import chainer
import chainer.functions as F
from chainer import Variable

from chainer import cuda

class ClassificationUpdater(chainer.training.StandardUpdater):
    def __init__(self, *args, **kwargs):
        self.model = kwargs.pop('models')
        self.xp = self.model.xp
        super(ClassificationUpdater, self).__init__(*args, **kwargs)


    # overraped?
    def update_core(self):
        # try:
        #     # gpu = -1
        #     # xp = np
        #     # gpu >= 0
        #     xp = cuda.cupy
        # except AttributeError:
        #     xp = np

        batch = self.get_iterator('main').next()
        batchsize = len(batch)
        x = []
        t = []
        for j in range(batchsize):
            x.append(batch[j][0].data)
            t.append(batch[j][1].data)
        points, target = Variable(self.xp.array(x)), Variable(self.xp.array(t))

        target = Variable(target.data[:,0])
        points = points.transpose([0, 2, 1])

        # print(points.debug_print())
        loss, accuracy = self.model(points, target)

        self.model.cleargrads()
        loss.backward()
        optimizer = self._optimizers['main']
        # optimizer = self.get_optimizers['main']
        optimizer.update()

        chainer.reporter.report({'train_loss': loss.data})
        # chainer.reporter.report({'accuracy': correct/float(size)})
        chainer.reporter.report({'accuracy': accuracy})
        # chainer.reporter.report({'accuracy': accuracy/float(size)})


class PartSegmentaionUpdater(chainer.training.StandardUpdater):
    def __init__(self, *args, **kwargs):
        self.model = kwargs.pop('models')
        self.xp = self.model.xp
        super(PartSegmentaionUpdater, self).__init__(*args, **kwargs)


    # overraped?
    def update_core(self):
        # try:
        #     # gpu = -1
        #     # xp = np
        #     # gpu >= 0
        #     xp = cuda.cupy
        # except AttributeError:
        #     xp = np

        batch = self.get_iterator('main').next()
        batchsize = len(batch)
        x = []
        t = []
        for j in range(batchsize):
            x.append(batch[j][0].data)
            t.append(batch[j][1].data)
        points, target = Variable(self.xp.array(x)), Variable(self.xp.array(t))

        points = points.transpose([0, 2, 1])
        loss, accuracy = self.model(points, target)

        self.model.cleargrads()
        loss.backward()
        optimizer = self._optimizers['main']
        # optimizer = self.get_optimizers['main']
        optimizer.update()

        chainer.reporter.report({'train_loss': loss.data})
        # chainer.reporter.report({'accuracy': correct/float(size)})
        chainer.reporter.report({'accuracy': accuracy})
        # chainer.reporter.report({'accuracy': accuracy/float(size)})



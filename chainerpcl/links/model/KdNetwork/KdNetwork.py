# -*- coding: utf-8 -*-
import chainer
import chainer.functions as F
import chainer.links as L
import numpy as np
import numpy.random
from chainer import Function, Variable

from chainerpcl.utils import download_model


class KDNet_Batch(chainer.Chain):
    """
    """
    def __init__(self):
        super(KDNet_Batch, self).__init__(
            conv1 = L.ConvolutionND(1,3,8 * 3,1,1),
            conv2 = L.ConvolutionND(1,8*2,32 * 3,1,1),
            conv3 = L.ConvolutionND(1,32*2,64 * 3,1,1),
            conv4 = L.ConvolutionND(1,64*2,64 * 3,1,1),
            conv5 = L.ConvolutionND(1,64*2,64 * 3,1,1),
            conv6 = L.ConvolutionND(1,64*2,128 * 3,1,1),
            conv7 = L.ConvolutionND(1,128*2,256 * 3,1,1),
            conv8 = L.ConvolutionND(1,256*2,512 * 3,1,1),
            conv9 = L.ConvolutionND(1,512*2,512 * 3,1,1),
            conv10 = L.ConvolutionND(1,512*2,512 * 3,1,1),
            conv11 = L.ConvolutionND(1,512*2,1024 * 3,1,1),
            bn1 = L.BatchNormalization(8*3),
            bn2 = L.BatchNormalization(32*3),
            bn3 = L.BatchNormalization(64*3),
            bn4 = L.BatchNormalization(64*3),
            bn5 = L.BatchNormalization(64*3),
            bn6 = L.BatchNormalization(128*3),
            bn7 = L.BatchNormalization(256*3),
            bn8 = L.BatchNormalization(512*3),
            bn9 = L.BatchNormalization(512*3),
            bn10 = L.BatchNormalization(512*3),
            bn11 = L.BatchNormalization(1024*3),
            fc = L.Linear(1024 * 2, k),
        )


    def __call__(self, x, y):
        return F.softmax_cross_entropy(self.fwd(x, y), y)


    def fwd(self, x, c):
        def kdconv(x, dim, featdim, sel, conv, bn):
            batchsize = x.size(0)
            # print(x.size())
            x =  F.relu(bn(conv(x)))
            # PyTorch
            # x = x.view(-1, featdim, 3, dim)
            # x = x.view(-1, featdim, 3 * dim)
            # x = x.transpose(1,0).contiguous()
            # x = x.view(featdim, 3 * dim * batchsize)
            x = x.reshape(-1, featdim, 3, dim)
            x = x.reshape(-1, featdim, 3 * dim)
            x = x.transpose(1, 0)
            x = x.reshape(featdim, 3 * dim * batchsize)

            #print x.size()
            # sel = Variable(sel + (torch.arange(0,dim) * 3).repeat(batchsize,1).long()).view(-1,1)
            #print sel.size()
            # offset = Variable((torch.arange(0,batchsize) * dim * 3).repeat(dim,1).transpose(1,0).contiguous().long().view(-1,1))
            # sel = sel + offset
            # Chainer
            tmp = self.xp.arange(0, dim, 3).repeat(batchsize,1)
            tmp = tmp.reshape(-1,1)
            sel = sel + tmp
            offset = self.xp.arange(0, batchsize, dim * 3).repeat(dim, 1)
            offset = offset.transpose([1, 0, 2])
            offset = offset.reshape(-1,1)
            sel = sel + offset

            # if x.is_cuda:
            #     sel = sel.cuda()
            # sel = sel.squeeze()
            # Chainer
            sel = sel.squeeze()
            # sel = self.xp.squeeze(sel)

            # x = torch.index_select(x, dim = 1, index = sel)
            # x = x.view(featdim, batchsize, dim)
            # x = x.transpose(1,0).contiguous()
            # x = x.transpose(2,1).contiguous()
            # x = x.view(-1, dim/2, featdim * 2)
            # x = x.transpose(2,1).contiguous()
            # Chainer
            x = self.xp.take(x, axis=1, indices=sel)
            x = x.reshape(featdim, batchsize, dim)
            x = x.transpose([1, 0, 2])
            x = x.transpose([0, 2, 1])
            x = x.reshape(-1, dim/2, featdim * 2)
            x = x.transpose([0, 2, 1])
            return x

        x1 = kdconv(x, 2048, 8, c[-1], self.conv1, self.bn1)
        x2 = kdconv(x1, 1024, 32, c[-2], self.conv2, self.bn2)
        x3 = kdconv(x2, 512, 64, c[-3], self.conv3, self.bn3)
        x4 = kdconv(x3, 256, 64, c[-4], self.conv4, self.bn4)
        x5 = kdconv(x4, 128, 64, c[-5], self.conv5, self.bn5)
        x6 = kdconv(x5, 64, 128, c[-6], self.conv6, self.bn6)
        x7 = kdconv(x6, 32, 256, c[-7], self.conv7, self.bn7)
        x8 = kdconv(x7, 16, 512, c[-8], self.conv8, self.bn8)
        x9 = kdconv(x8, 8, 512, c[-9], self.conv9, self.bn9)
        x10 = kdconv(x9, 4, 512, c[-10], self.conv10, self.bn10)
        x11 = kdconv(x10, 2, 1024, c[-11], self.conv11, self.bn11)
        x11 = x11.view(-1,1024 * 2)
        # out = F.log_softmax(self.fc(x11))
        out = self.fc(x11)
        return out


class KDNet_Batch_mp_4d(chainer.Chain):
    def __init__(self, k = 16):
        super(KDNet_Batch, self).__init__(
            conv1 = L.ConvolutionND(1,4,8 * 3,1,1),
            conv2 = L.ConvolutionND(1,8,32 * 3,1,1),
            conv3 = L.ConvolutionND(1,32,64 * 3,1,1),
            conv4 = L.ConvolutionND(1,64,64 * 3,1,1),
            conv5 = L.ConvolutionND(1,64,64 * 3,1,1),
            conv6 = L.ConvolutionND(1,64,128 * 3,1,1),
            conv7 = L.ConvolutionND(1,128,256 * 3,1,1),
            conv8 = L.ConvolutionND(1,256,512 * 3,1,1),
            conv9 = L.ConvolutionND(1,512,512 * 3,1,1),
            conv10 = L.ConvolutionND(1,512,512 * 3,1,1),
            conv11 = L.ConvolutionND(1,512,1024 * 3,1,1),
            bn1 = L.BatchNormalization(8*3),
            bn2 = L.BatchNormalization(32*3),
            bn3 = L.BatchNormalization(64*3),
            bn4 = L.BatchNormalization(64*3),
            bn5 = L.BatchNormalization(64*3),
            bn6 = L.BatchNormalization(128*3),
            bn7 = L.BatchNormalization(256*3),
            bn8 = L.BatchNormalization(512*3),
            bn9 = L.BatchNormalization(512*3),
            bn10 = L.BatchNormalization(512*3),
            bn11 = L.BatchNormalization(1024*3),
            fc = L.Linear(1024, k),
        )


    def __call__(self, x, y):
        return F.softmax_cross_entropy(self.fwd(x, y), y)


    def fwd(self, x, c):
        def kdconv(x, dim, featdim, sel, conv, bn):
            batchsize = x.size(0)
            x =  F.relu(bn(conv(x)))
            x = x.view(-1, featdim, 3, dim)
            x = x.view(-1, featdim, 3 * dim)
            # x = x.transpose(1, 0)
            x = x.transpose([1, 0, 2])
            x = x.view(featdim, 3 * dim * batchsize)
            # #print x.size()
            # sel = Variable(sel + (torch.arange(0,dim) * 3).repeat(batchsize,1).long()).view(-1,1)
            # #print sel.size()
            # offset = Variable((torch.arange(0,batchsize) * dim * 3).repeat(dim,1).transpose(1,0).contiguous().long().view(-1,1))
            # sel = sel+offset
            # Chainer
            tmp = self.xp.arange(0, dim, 3).repeat(batchsize,1)
            tmp = tmp.reshape(-1,1)
            sel = sel + tmp
            offset = self.xp.arange(0, batchsize, dim * 3).repeat(dim, 1)
            offset = offset.transpose([1, 0, 2])
            offset = offset.reshape(-1,1)
            sel = sel + offset

            # if x.is_cuda:
            #     sel = sel.cuda()
            # sel = sel.squeeze()
            sel = self.xp.squeeze(sel)

            # x = torch.index_select(x, dim = 1, index = sel)
            # x = x.view(featdim, batchsize, dim)
            # x = x.transpose(1,0).contiguous()
            # x = x.view(-1, featdim, dim/2, 2)
            # x = torch.squeeze(torch.max(x, dim = -1)[0], 3)
            # Chainer
            x = self.xp.take(x, axis=1, indices=sel)
            x = x.reshape(featdim, batchsize, dim)
            x = x.transpose([1, 0, 2])
            x = x.reshape(-1, featdim, dim/2, 2)
            x = self.xp.squeeze(self.xp.argmax(x, axis=0), 3)
            return x

        x1 = kdconv(x, 2048, 8, c[-1], self.conv1, self.bn1)
        x2 = kdconv(x1, 1024, 32, c[-2], self.conv2, self.bn2)
        x3 = kdconv(x2, 512, 64, c[-3], self.conv3, self.bn3)
        x4 = kdconv(x3, 256, 64, c[-4], self.conv4, self.bn4)
        x5 = kdconv(x4, 128, 64, c[-5], self.conv5, self.bn5)
        x6 = kdconv(x5, 64, 128, c[-6], self.conv6, self.bn6)
        x7 = kdconv(x6, 32, 256, c[-7], self.conv7, self.bn7)
        x8 = kdconv(x7, 16, 512, c[-8], self.conv8, self.bn8)
        x9 = kdconv(x8, 8, 512, c[-9], self.conv9, self.bn9)
        x10 = kdconv(x9, 4, 512, c[-10], self.conv10, self.bn10)
        x11 = kdconv(x10, 2, 1024, c[-11], self.conv11, self.bn11)
        x11 = x11.view(-1,1024)
        # out = F.log_softmax(self.fc(x11))
        out =self.fc(x11)
        return out


class KDNet_Batch_mp(chainer.Chain):
    def __init__(self, k = 16):
        super(KDNet_Batch, self).__init__(
            conv1 = L.ConvolutionND(1,3,8 * 3,1,1),
            conv2 = L.ConvolutionND(1,8,32 * 3,1,1),
            conv3 = L.ConvolutionND(1,32,64 * 3,1,1),
            conv4 = L.ConvolutionND(1,64,64 * 3,1,1),
            conv5 = L.ConvolutionND(1,64,64 * 3,1,1),
            conv6 = L.ConvolutionND(1,64,128 * 3,1,1),
            conv7 = L.ConvolutionND(1,128,256 * 3,1,1),
            conv8 = L.ConvolutionND(1,256,512 * 3,1,1),
            conv9 = L.ConvolutionND(1,512,512 * 3,1,1),
            conv10 = L.ConvolutionND(1,512,512 * 3,1,1),
            conv11 = L.ConvolutionND(1,512,1024 * 3,1,1),
            bn1 = L.BatchNormalization(8*3),
            bn2 = L.BatchNormalization(32*3),
            bn3 = L.BatchNormalization(64*3),
            bn4 = L.BatchNormalization(64*3),
            bn5 = L.BatchNormalization(64*3),
            bn6 = L.BatchNormalization(128*3),
            bn7 = L.BatchNormalization(256*3),
            bn8 = L.BatchNormalization(512*3),
            bn9 = L.BatchNormalization(512*3),
            bn10 = L.BatchNormalization(512*3),
            bn11 = L.BatchNormalization(1024*3),
            fc = L.Linear(1024, k),
        )


    def __call__(self, x, y):
        return F.softmax_cross_entropy(self.fwd(x, y), y)


    def fwd(self, x, c):
        def kdconv(x, dim, featdim, sel, conv, bn):
            # batchsize = x.size(0)
            # x = F.relu(bn(conv(x)))
            # x = x.view(-1, featdim, 3, dim)
            # x = x.view(-1, featdim, 3 * dim)
            # x = x.transpose(1,0).contiguous()
            # x = x.view(featdim, 3 * dim * batchsize)
            # #print x.size()
            # sel = Variable(sel + (torch.arange(0,dim) * 3).repeat(batchsize,1).long()).view(-1,1)
            # #print sel.size()
            # offset = Variable((torch.arange(0,batchsize) * dim * 3).repeat(dim,1).transpose(1,0).contiguous().long().view(-1,1))
            # sel = sel+offset
            # Chainer
            batchsize = x.size[0]
            x =  F.relu(bn(conv(x)))
            x = x.reshape(-1, featdim, 3, dim)
            x = x.reshape(-1, featdim, 3 * dim)
            x = x.transpose([1, 0, 2])
            x = x.reshape(featdim, 3 * dim * batchsize)
            tmp = self.xp.arange(0, dim, 3).repeat(batchsize, 1)
            tmp = tmp.reshape(-1,1)
            sel = sel + tmp
            offset = self.xp.arange(0, batchsize, dim * 3).repeat(dim, 1)
            offset = offset.transpose([1, 0, 2])
            offset = offset.reshape(-1,1)
            sel = sel + offset

            # if x.is_cuda:
            #     sel = sel.cuda()
            # sel = sel.squeeze()
            sel = sel.squeeze()

            # x = torch.index_select(x, dim = 1, index = sel)
            # x = x.view(featdim, batchsize, dim)
            # x = x.transpose(1,0).contiguous()
            # x = x.view(-1, featdim, dim/2, 2)
            # x = torch.squeeze(torch.max(x, dim = -1)[0], 3)
            # Chainer
            x = self.xp.take(x, axis=1, indices=sel)
            x = x.reshape(featdim, batchsize, dim)
            x = x.transpose([1, 0, 2])
            x = x.reshape(-1, featdim, dim/2, 2)
            x = self.xp.squeeze(self.xp.argmax(x, axis=0), 3)
            return x

        x1 = kdconv(x, 2048, 8, c[-1], self.conv1, self.bn1)
        x2 = kdconv(x1, 1024, 32, c[-2], self.conv2, self.bn2)
        x3 = kdconv(x2, 512, 64, c[-3], self.conv3, self.bn3)
        x4 = kdconv(x3, 256, 64, c[-4], self.conv4, self.bn4)
        x5 = kdconv(x4, 128, 64, c[-5], self.conv5, self.bn5)
        x6 = kdconv(x5, 64, 128, c[-6], self.conv6, self.bn6)
        x7 = kdconv(x6, 32, 256, c[-7], self.conv7, self.bn7)
        x8 = kdconv(x7, 16, 512, c[-8], self.conv8, self.bn8)
        x9 = kdconv(x8, 8, 512, c[-9], self.conv9, self.bn9)
        x10 = kdconv(x9, 4, 512, c[-10], self.conv10, self.bn10)
        x11 = kdconv(x10, 2, 1024, c[-11], self.conv11, self.bn11)
        x11 = x11.view(-1,1024)
        # out = F.log_softmax(self.fc(x11))
        out = self.fc(x11)
        return out
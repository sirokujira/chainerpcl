# -*- coding: utf-8 -*-
import chainer
import chainer.functions as F
import chainer.links as L
import numpy as np
import cupy
import numpy.random
from chainer import Function, Variable

from chainer import cuda

from chainerpcl.utils import download_model


class STN3d(chainer.Chain):
    def __init__(self, num_points = 2500, k = 1, pad=0):
        super(STN3d, self).__init__(
            conv1 = L.ConvolutionND(1, in_channels=3, out_channels=64, ksize=1, pad=0, nobias=False),
            conv2 = L.ConvolutionND(1, in_channels=64, out_channels=128, ksize=1, pad=0, nobias=False),
            conv3 = L.ConvolutionND(1, in_channels=128, out_channels=1024, ksize=1, pad=0, nobias=False),

            # mp1 = F.max_pooling_nd(num_points, ksize=1)
            # fc1 = LinearBlock(1024, 512)
            # fc2 = LinearBlock(512, 256)
            # fc3 = LinearBlock(256, 9)
            fc1 = L.Linear(1024, 512),
            fc2 = L.Linear(512, 256),
            fc3 = L.Linear(256, 9),
            # relu = ReLU()

            bn1 = L.BatchNormalization(64),
            bn2 = L.BatchNormalization(128),
            bn3 = L.BatchNormalization(1024),
            bn4 = L.BatchNormalization(512),
            bn5 = L.BatchNormalization(256),
        )
        self.num_points = num_points


    def __call__(self, x, y):
        return F.softmax_cross_entropy(self.fwd(x), y)


    def fwd(self, x):
        # print(x.data[0])
        batchsize = len(x.data[:])
        # batchsize = 32
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.bn3(self.conv3(x)))
        # print('x: \n')
        # 32x1024x2500
        # print(x.debug_print())

        # NG : 32x1024x1
        # x = F.max_pooling_nd(x, ksize=(1), stride=2500)
        # x = F.max_pooling_nd(x, ksize=1, stride=self.num_points, cover_all=False)
        x = F.max_pooling_nd(x, ksize=1, stride=self.num_points, cover_all=False)

        # 32x1024x1
        # print('mp: \n')
        # print(x.debug_print())

        # Shape change
        # PyTorch(view)
        # x = x.view(-1, 1024)
        x = x.reshape(-1, 1024)

        # 32x1024
        # print('reshape: \n')
        # print(x.debug_print())

        x = F.relu(self.bn4(self.fc1(x)))
        x = F.relu(self.bn5(self.fc2(x)))
        x = self.fc3(x)
        # [32 * 9]

        # PyTorch(view)
        # iden = Variable(np.array([1,0,0,0,1,0,0,0,1]).astype(np.float32)).view(1,9).repeat(batchsize, 1)
        # iden = Variable(np.array([1,0,0,0,1,0,0,0,1]).astype(np.float32)).reshape(1,9).repeat(batchsize, 1)
        # iden = Variable(np.array([1,0,0,0,1,0,0,0,1]).reshape(1,9).repeat(batchsize, 1).astype(np.float32))
        # iden = Variable(np.array([1,0,0,0,1,0,0,0,1], dtype=np.float32).reshape(1,9).repeat(batchsize, 1))
        # Chainer
        # data = np.array([1,0,0,0,1,0,0,0,1], dtype=np.float32).reshape(1, 9)
        # 3.0.0?
        data = self.xp.array([1,0,0,0,1,0,0,0,1], dtype=self.xp.float32).reshape(1, 9)
        # print(data)
        # data = np.repeat(data, batchsize, 0)
        data = self.xp.repeat(data, batchsize, 0)
        # print(data)
        iden = Variable(data)

        # print(iden)
        # print(iden.debug_print())
        x = x + iden
        # x = x.view(-1, 3, 3)
        x = x.reshape(-1, 3, 3)
        return x


class PointNetfeat(chainer.Chain):
    def __init__(self, num_points = 2500, global_feat = True):
        self.num_points = num_points
        self.global_feat = global_feat
        super(PointNetfeat, self).__init__(
            stn = STN3d(num_points = self.num_points),
            conv1 = L.ConvolutionND(1, in_channels=3, out_channels=64, ksize=1, pad=0, nobias=False),
            conv2 = L.ConvolutionND(1, in_channels=64, out_channels=128, ksize=1, pad=0, nobias=False),
            conv3 = L.ConvolutionND(1, in_channels=128, out_channels=1024, ksize=1, pad=0, nobias=False),
            bn1 = L.BatchNormalization(64),
            bn2 = L.BatchNormalization(128),
            bn3 = L.BatchNormalization(1024),
            # mp1 = F.max_pooling_nd(self.num_points, 1, cover_all=False)
        )


    def __call__(self, x, y):
        pred, _ = self.fwd(x)
        return F.softmax_cross_entropy(pred, y)


    def fwd(self, x):
        # batchsize = x.data[0]
        batchsize = len(x.data[:])
        # batchsize = 32
        trans = self.stn.fwd(x)

        # 32x3x2500
        # print(x)
        # x = x.transpose(2,1)
        # x = Variable(np.transpose(x, (1, 0)))
        x = x.transpose([0, 2, 1])

        # 
        # x = torch.bmm(x, trans)
        # chainer.functions.batch_matmul?
        # https://docs.chainer.org/en/stable/reference/generated/chainer.functions.batch_matmul.html?highlight=functions.matmul
        # x = 32x2500x3
        # trans = (32*3*3)
        # x = x * trans
        x = F.batch_matmul(x, trans)

        # x = x.transpose(2,1)
        x = x.transpose([0, 2, 1])

        x = F.relu(self.bn1(self.conv1(x)))
        pointfeat = x
        x = F.relu(self.bn2(self.conv2(x)))
        x = self.bn3(self.conv3(x))

        # print(self.num_points)
        x = F.max_pooling_nd(x, 1, stride=self.num_points, cover_all=False)

        # x = x.view(-1, 1024)
        x = x.reshape(-1, 1024)

        if self.global_feat:
            return x, trans
        else:
            # print('global_feat = False.\n')
            # 32x1024
            # print(x.debug_print())

            # PyTorch
            # 32*1, 1024*1, 1 * self.num_points
            # x = x.view(-1, 1024, 1).repeat(1, 1, self.num_points)
            x = x.reshape(-1, 1024, 1)
            # 32x1024x1
            # print(x.debug_print())
            # x = Variable(np.repeat(x, self.num_points, axis=2))
            # x = np.repeat(x, self.num_points, axis=2)
            x = self.xp.repeat(x.data, self.num_points, axis=2)
            x = Variable(x)

            # 32x1024x2500
            # print(x.debug_print())
            # 32x64x2500
            # print(pointfeat.debug_print())

            # http://pytorch.org/docs/master/torch.html?highlight=torch%20cat#torch.cat
            # seq, dim
            # 32x1088x2500
            # return torch.cat([x, pointfeat], 1), trans
            return F.concat((x, pointfeat), axis=1), trans


class PointNetCls(chainer.Chain):
    """PointNet Basic for classification.

    This is a PointNet [#]_ model for semantic segmenation. This is based on
    PointNetBasic model that is found here_.

    When you specify the path of a pretrained chainer model serialized as
    a :obj:`.npz` file in the constructor, this chain model automatically
    initializes all the parameters with it.
    When a string in prespecified set is provided, a pretrained model is
    loaded from weights distributed on the Internet.
    The list of pretrained models supported are as follows:

    * :obj:`shapenet3d`: Loads weights trained with the train split of \
        ShapeNet dataset.

    .. [#] Vijay Badrinarayanan, Alex Kendall and Roberto Cipolla "PointNet: A \
    Deep Convolutional Encoder-Decoder Architecture for Image Segmentation." \
    PAMI, 2017

    .. _here: http://github.com/alexgkendall/PointNet-Tutorial

    Args:
        n_class (int): The number of classes. If :obj:`None`, it can
            be infered if :obj:`pretrained_model` is given.
        pretrained_model (str): The destination of the pretrained
            chainer model serialized as a :obj:`.npz` file.
            If this is one of the strings described
            above, it automatically loads weights stored under a directory
            :obj:`$CHAINER_DATASET_ROOT/pfnet/chainerpcl/models/`,
            where :obj:`$CHAINER_DATASET_ROOT` is set as
            :obj:`$HOME/.chainer/dataset` unless you specify another value
            by modifying the environment variable.
        initialW (callable): Initializer for convolution layers.

    """

    _models = {
        'shapenet': {
            'n_class': 10,
            'url': 'https://github.com/sirokujira/share-weights/releases/'
            'download/0.0.1/pointnet_shapenet3d_20_??_??.npz'
        }
    }
    def __init__(self, num_points = 2500, k = 5, pad=1, nobias=False):
        self.num_points = num_points
        self.k = k
        super(PointNetCls, self).__init__(
            # with self.init_scope():
            feat = PointNetfeat(num_points = self.num_points, global_feat=True),
            fc1 = L.Linear(1024, 512),
            fc2 = L.Linear(512, 256),
            # fc3 = L.Linear(256, self.k),
            fc3 = L.Linear(256, k),
            bn1 = L.BatchNormalization(512),
            bn2 = L.BatchNormalization(256),
        )


    def __call__(self, x, y):
        pred, _ = self.fwd(x)
        pred_choice = pred.data.argmax(1)
        correct = pred_choice.__eq__(y.data).sum()
        # accuracy = correct/float(opt.batchsize)
        accuracy = correct/float(32)
        return F.softmax_cross_entropy(pred, y), Variable(self.xp.array(accuracy, dtype=self.xp.float32))


    def fwd(self, x):
        x, trans = self.feat.fwd(x)
        x = F.relu(self.bn1(self.fc1(x)))
        x = F.relu(self.bn2(self.fc2(x)))
        x = self.fc3(x)
        # return F.log_softmax(x), trans
        # call F.softmax_cross_entropy
        return x, trans


    def predict(self, points):
        """Detect objects from PointDatas.

        This method predicts objects for each points.

        Args:
            points (iterable of numpy.ndarray): Arrays holding PointDatas.
                                                All points are in XYZ format.

        Returns:
            list of numpy.ndarray:

            List of integer labels predicted from each image in the input \
            list.
        """
        labels = list()
        for point in points:
            x, y, z = point.shape
            with chainer.using_config('train', False), \
                    chainer.function.no_backprop_mode():
                x = chainer.Variable(self.xp.asarray(point))
                # x = chainer.Variable(self.xp.asarray(point[self.xp.newaxis]))
                score = self.fwd(x)[0].data
                # score = self.fwd(x)[1].data

            score = chainer.cuda.to_cpu(score)
            # print(score)
            label = self.xp.argmax(score, axis=1).astype(self.xp.int32)
            # print(label)
            labels.append(label)

        return labels


class PointNetDenseCls(chainer.Chain):
    def __init__(self, num_points = 2500, k = 2):
        self.num_points = num_points
        self.k = k
        super(PointNetDenseCls, self).__init__(
            feat = PointNetfeat(num_points = self.num_points, global_feat=False),
            conv1 = L.ConvolutionND(1, in_channels=1088, out_channels=512, ksize=1, pad=0, nobias=False),
            conv2 = L.ConvolutionND(1, in_channels=512, out_channels=256, ksize=1, pad=0, nobias=False),
            conv3 = L.ConvolutionND(1, in_channels=256, out_channels=128, ksize=1, pad=0, nobias=False),
            conv4 = L.ConvolutionND(1, in_channels=128, out_channels=self.k, ksize=1, pad=0, nobias=False),
            bn1 = L.BatchNormalization(512),
            bn2 = L.BatchNormalization(256),
            bn3 = L.BatchNormalization(128),
        )


    def __call__(self, x, y):
        pred, _ = self.fwd(x)

        pred = pred.reshape(-1, self.k)
        y = y.reshape(-1,1)[:,0] - 1

        pred_choice = pred.data.argmax(1)
        # print(pred_choice.shape)
        # print(y.data.shape)
        correct = pred_choice.__eq__(y.data).sum()
        # accuracy = correct/float(opt.batchsize * self.num_points)
        accuracy = correct/float(32 * self.num_points)
        return F.softmax_cross_entropy(pred, y), Variable(self.xp.array(accuracy, dtype=self.xp.float32))


    def fwd(self, x):
        # batchsize = x.size()[0]
        batchsize = len(x.data[:])
        # print('batchsize: \n')
        # print(batchsize)

        x, trans = self.feat.fwd(x)
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.bn3(self.conv3(x)))
        x = self.conv4(x)
        # PyTorch
        # x = x.transpose(2,1).contiguous()
        # Chainer
        x = x.transpose([0, 2, 1])
        # print(x.debug_print())
        # x = x.contiguous()

        # PyTorch
        # x = F.log_softmax(x.view(-1, self.k))
        x = F.log_softmax(x.reshape(-1, self.k))
        # print(x.debug_print())

        # PyTorch
        # x = x.view(batchsize, self.num_points, self.k)
        x = x.reshape(batchsize, self.num_points, self.k)
        # print(x.debug_print())

        return x, trans


    def predict(self, points):
        """Detect objects from PointDatas.

        This method predicts objects for each points.

        Args:
            points (iterable of numpy.ndarray): Arrays holding PointDatas.
                                                All points are in XYZ format.

        Returns:
            list of numpy.ndarray:

            List of integer labels predicted from each image in the input \
            list.
        """
        labels = list()
        for point in points:
            x, y, z = point.shape
            with chainer.using_config('train', False), \
                    chainer.function.no_backprop_mode():
                x = chainer.Variable(self.xp.asarray(point))
                # x = chainer.Variable(self.xp.asarray(point[self.xp.newaxis]))
                score = self.fwd(x)[0].data

            score = chainer.cuda.to_cpu(score)
            # print(score)
            label = self.xp.argmax(score, axis=1).astype(self.xp.int32)
            # print(label)
            labels.append(label)

        return labels

if __name__ == '__main__':
    sim_data = numpy.random.randn(32, 3, 2500)
    sim_data = self.xp.array(sim_data, dtype=self.xp.float32)
    sim_data = Variable(sim_data)
    print(sim_data.debug_print())
    # print(sim_data)
    # print(sim_data[0])
    trans = STN3d()
    out = trans.fwd(sim_data)
    # 32x3x3
    print('stn\n')
    print(out.debug_print())

    pointfeat = PointNetfeat(global_feat=True)
    out, _ = pointfeat.fwd(sim_data)
    # print('global feat', out.size())
    # 32x1024
    print('global feat(True)\n')
    print(out.debug_print())

    pointfeat = PointNetfeat(global_feat=False)
    out, _ = pointfeat.fwd(sim_data)
    # print('point feat', out.size())
    # 32x1088x2500
    print('point feat(False)\n')
    print(out.debug_print())

    cls = PointNetCls(k = 5)
    out, _ = cls.fwd(sim_data)
    # print('class', out.size())
    # 32x5
    print('class\n')
    print(out.debug_print())

    seg = PointNetDenseCls(k = 3)
    out, _ = seg.fwd(sim_data)
    # print('seg', out.size())
    # 32x2500x3
    print('seg\n')
    print(out.debug_print())



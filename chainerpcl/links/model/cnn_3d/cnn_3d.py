# -*- coding: utf-8 -*-
import chainer
import chainer.functions as F
import chainer.links as L
import numpy as np
import cupy
import numpy.random
from chainer import Function, Variable

from chainer import cuda

from chainerpcl.utils import download_model


class CNN_3D(chainer.Chain):
    def __init__(self, num_points = 2500, k = 1, pad=0):
        super(CNN_3D, self).__init__(
            conv1 = L.ConvolutionND(1, in_channels=3, out_channels=64, ksize=1, pad=0, nobias=False),
            conv2 = L.ConvolutionND(1, in_channels=64, out_channels=128, ksize=1, pad=0, nobias=False),
            conv3 = L.ConvolutionND(1, in_channels=128, out_channels=1024, ksize=1, pad=0, nobias=False),

            # mp1 = F.max_pooling_nd(num_points, ksize=1)
            # fc1 = LinearBlock(1024, 512)
            # fc2 = LinearBlock(512, 256)
            # fc3 = LinearBlock(256, 9)
            fc1 = L.Linear(1024, 512),
            fc2 = L.Linear(512, 256),
            fc3 = L.Linear(256, 9),
            # relu = ReLU()

            bn1 = L.BatchNormalization(64),
            bn2 = L.BatchNormalization(128),
            bn3 = L.BatchNormalization(1024),
            bn4 = L.BatchNormalization(512),
            bn5 = L.BatchNormalization(256),
        )
        self.num_points = num_points


    def __call__(self, x, y):
        return F.softmax_cross_entropy(self.fwd(x), y)


    def fwd(self, x):
        # print(x.data[0])
        batchsize = len(x.data[:])
        # batchsize = 32
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.bn3(self.conv3(x)))
        # print('x: \n')
        # 32x1024x2500
        # print(x.debug_print())

        # NG : 32x1024x1
        # x = F.max_pooling_nd(x, ksize=(1), stride=2500)
        # x = F.max_pooling_nd(x, ksize=1, stride=self.num_points, cover_all=False)
        x = F.max_pooling_nd(x, ksize=1, stride=self.num_points, cover_all=False)

        # 32x1024x1
        # print('mp: \n')
        # print(x.debug_print())

        # Shape change
        # PyTorch(view)
        # x = x.view(-1, 1024)
        x = x.reshape(-1, 1024)

        # 32x1024
        # print('reshape: \n')
        # print(x.debug_print())

        x = F.relu(self.bn4(self.fc1(x)))
        x = F.relu(self.bn5(self.fc2(x)))
        x = self.fc3(x)
        # [32 * 9]

        # PyTorch(view)
        # iden = Variable(np.array([1,0,0,0,1,0,0,0,1]).astype(np.float32)).view(1,9).repeat(batchsize, 1)
        # iden = Variable(np.array([1,0,0,0,1,0,0,0,1]).astype(np.float32)).reshape(1,9).repeat(batchsize, 1)
        # iden = Variable(np.array([1,0,0,0,1,0,0,0,1]).reshape(1,9).repeat(batchsize, 1).astype(np.float32))
        # iden = Variable(np.array([1,0,0,0,1,0,0,0,1], dtype=np.float32).reshape(1,9).repeat(batchsize, 1))
        # Chainer
        # data = np.array([1,0,0,0,1,0,0,0,1], dtype=np.float32).reshape(1, 9)
        data = self.xp.array([1,0,0,0,1,0,0,0,1], dtype=self.xp.float32).reshape(1, 9)
        # print(data)
        # data = np.repeat(data, batchsize, 0)
        data = self.xp.repeat(data, batchsize, 0)
        # print(data)
        iden = Variable(data)

        # print(iden)
        # print(iden.debug_print())
        x = x + iden
        # x = x.view(-1, 3, 3)
        x = x.reshape(-1, 3, 3)
        return x


if __name__ == '__main__':
    sim_data = numpy.random.randn(32, 3, 2500)
    sim_data = self.xp.array(sim_data, dtype=self.xp.float32)
    sim_data = Variable(sim_data)
    print(sim_data.debug_print())
    # print(sim_data)
    # print(sim_data[0])
    trans = CNN_3D()
    out = trans.fwd(sim_data)
    # 32x3x3
    print('CNN_3D\n')
    print(out.debug_print())



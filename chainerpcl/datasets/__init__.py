from chainerpcl.datasets.directory_parsing_label_dataset import directory_parsing_label_names   # NOQA
from chainerpcl.datasets.directory_parsing_label_dataset import DirectoryParsingLabelDataset    # NOQA
# from chainerpcl.datasets.kitti.kitti_bbox_dataset import KITTIBboxDataset                       # NOQA
# from chainerpcl.datasets.kitti.kitti_utils import kitti_bbox_label_colors                       # NOQA
# from chainerpcl.datasets.kitti.kitti_utils import kitti_bbox_label_names                        # NOQA
# from chainerpcl.datasets.kitti.kitti_utils import kitti_ignore_bbox_label_color                 # NOQA
# from chainerpcl.datasets.kitti.parseTrackletXML import parseXML                                 # NOQA
from chainerpcl.datasets.mixup_soft_label_dataset import MixUpSoftLabelDataset  # NOQA
# NYU_V2
from chainerpcl.datasets.nyuv2 import NYU_V2_dataset
# S3DIS(http://buildingparser.stanford.edu/dataset.html)
# https://github.com/alexsax/2D-3D-Semantics
# before download Archive files
# Reason : Upload Google Drive
# from chainerpcl.datasets.s3dis import S3DIS_Dataset                                           # NOQA
# Semantic3D
# ShapeNet
# benckmark?
from chainerpcl.datasets.shapenet.BenchMarkDataset import BenchMarkDataset                      # NOQA
from chainerpcl.datasets.shapenet.BenchMarkDataset import PrincetonMODEL_label_10_names         # NOQA
from chainerpcl.datasets.shapenet.BenchMarkDataset import PrincetonMODEL_label_10_colors        # NOQA
from chainerpcl.datasets.shapenet.BenchMarkDataset import PrincetonMODEL_label_40_names         # NOQA
from chainerpcl.datasets.shapenet.BenchMarkDataset import PrincetonMODEL_label_40_colors        # NOQA
# Shapenet Core_v2
# Test PointNet
from chainerpcl.datasets.shapenet.shapenetcorev2_dataset import ShapeNetCorev2Dataset           # NOQA
from chainerpcl.datasets.shapenet.shapenetcorev2_dataset import ShapeNetCorev2_label_names      # NOQA
from chainerpcl.datasets.shapenet.shapenetcorev2_dataset import ShapeNetCorev2_label_colors     # NOQA
# ShizuokaDB(las)
from chainerpcl.datasets.shizuokadb.ShizuokaDB_dataset import ShizuokaDBDataset                 # NOQA
from chainerpcl.datasets.shizuokadb.shizuokadb_utils import get_ShizuokaDB_Data                 # NOQA
from chainerpcl.datasets.siamese_dataset import SiameseDataset                                  # NOQA
from chainerpcl.datasets.transform_dataset import TransformDataset                              # NOQA



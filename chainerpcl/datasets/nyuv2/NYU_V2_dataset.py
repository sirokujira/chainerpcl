import glob
import os
import shutil
from urllib.parse import urljoin

import numpy as np

import chainer
from chainer.dataset import download
from chainerpcl import utils
# from chainerpcl.utils import read_pointcloud


root = 'pfnet/chainerpcl/NYU_V2'
url_part_base = 'http://horatio.cs.nyu.edu/mit/silberman/nyu_depth_v2/'
url_all_part = 'http://horatio.cs.nyu.edu/mit/silberman/nyu_depth_v2/nyu_depth_v2_raw.zip'

NYU_V2_label_names = (
    'wall',
    'floor',
    'cabinet',
    'bed',
    'chair',
    'sofa',
    'table',
    'door',
    'window',
    'bookshelf',
    'picture',
    'counter',
    'blinds',
    'desk',
    'shelves',
    'curtain',
    'dresser',
    'pillow',
    'mirror',
    'floor mat',
    'clothes',
    'ceiling',
    'books',
    'refridgerator',
    'television',
    'paper',
    'towel',
    'shower curtain',
    'box',
    'whiteboard',
    'person',
    'night stand',
    'toilet',
    'sink',
    'lamp',
    'bathtub',
    'bag',
    'otherstructure',
    'otherfurniture',
    'otherprop',
)

NYU_V2_label_colors = (
    (128, 128, 128),
    (128, 0, 0),
    (192, 192, 128),
    (128, 64, 128),
    (60, 40, 222),
    (128, 128, 0),
    (192, 128, 128),
    (64, 64, 128),
    (64, 0, 128),
    (64, 64, 0),
    (0, 128, 192),
)
NYU_V2_ignore_label_color = (0, 0, 0)


class NYU_V2Dataset(chainer.dataset.DatasetMixin):

    """Dataset class for a semantic segmantion task on NYU_V2 `u`_.

    .. _`u`: https://cs.nyu.edu/~silberman/datasets/nyu_depth_v2.html

    Args:
        data_dir (string): Path to the root of the training data. If this is
            :obj:`auto`, this class will automatically download data for you
            under :obj:`$CHAINER_DATASET_ROOT/pfnet/chainerpcl/NYU_V2`.
        split ({'train', 'val', 'test'}): Select from dataset splits used
            in NYU_V2 Dataset.

    """

    def __init__(self, data_dir='auto', part_name=None):
        if data_dir == 'auto':
            if part_name == None:
                data_dir = get_NYU_V2_All('pfnet/chainerpcl/NYU_V2')
            else:
                data_dir = get_NYU_V2_Part('pfnet/chainerpcl/NYU_V2', part_name)

        img_list_filename = os.path.join(data_dir, '{}.txt'.format(split))
        self.filenames = [
            [os.path.join(data_dir, fn.replace('NYU_V2/', ''))
             for fn in line.split()] for line in open(img_list_filename)]


    def get_NYU_V2_Part(root, name):
        data_root = download.get_dataset_directory(root)
        url_data = urljoin(url_part_base, name + '.zip')
        download_file_path = utils.cached_download(url_data)
        ext = os.path.splitext(url_data)[1]
        utils.extractall(download_file_path, data_root, ext)
        return data_root


    def get_NYU_V2_All(root):
        data_root = download.get_dataset_directory(root)
        download_file_path = utils.cached_download(url_all_part)
        ext = os.path.splitext(url_all_part)[1]
        utils.extractall(download_file_path, data_root, ext)
        return data_root


    def get_NYU_V2_Image(self, data_root, part_name):
        folder = date + '_drive_' + driveNo
        if part_name == None:
            img_dir = os.path.join(data_root, os.path.join(date, folder + '_sync', 'image_' + ImgNo, 'data'))
        else:
            img_dir = os.path.join(data_root, os.path.join(date, folder + '_sync', 'image_' + ImgNo, 'data'))

        self.img_paths = list()
        if not os.path.exists(img_dir):
            raise ValueError(
                'NYU_V2 dataset does not exist at the expected location.'
                'Please download it from https://cs.nyu.edu/~silberman/datasets/nyu_depth_v2.html'
                'Then place directory image at {}.'.format(img_dir))

        for img_path in sorted(glob.glob(os.path.join(img_dir, '*.png'))):
                self.img_paths.append(img_path)
        pass


    def get_NYU_V2_Point(self, data_root, part_name):
        if part_name == None:
            point_dir = os.path.join(data_root, os.path.join(date, date + '_drive_' + driveNo + '_sync', 'velodyne_points', 'data'))
        else:
            point_dir = os.path.join(data_root, os.path.join(date, date + '_drive_' + driveNo + '_sync', 'velodyne_points', 'data'))

        self.point_paths = list()
        if not os.path.exists(point_dir):
            raise ValueError(
                'NYU_V2 dataset does not exist at the expected location.'
                'Please download it from https://cs.nyu.edu/~silberman/datasets/nyu_depth_v2.html'
                'Then place directory image at {}.'.format(point_dir))

        for point_path in sorted(glob.glob(os.path.join(point_dir, '*.bin'))):
                self.point_paths.append(point_path)

        return data_root


    def __len__(self):
        return len(self.filenames)


    def get_example(self, i):
        """Returns the i-th example.

        Returns a color image and a label image. The color image is in CHW
        format and the label image is in HW format.

        Args:
            i (int): The index of the example.

        Returns:
            tuple of a color image and a label whose shapes are (3, H, W) and
            (H, W) respectively. H and W are height and width of the image.
            The dtype of the color image is :obj:`numpy.float32` and
            the dtype of the label image is :obj:`numpy.int32`.

        """
        if i >= len(self):
            raise IndexError('index is too large')
        img_filename, label_filename = self.filenames[i]
        img = read_image(img_filename, color=True)
        label = read_image(label_filename, dtype=np.int32, color=False)[0]
        # Label id 11 is for unlabeled pixels.
        label[label == 11] = -1
        return img, label


    # https://stackoverflow.com/questions/35723865/read-a-pgm-file-in-python
    # https://www.mm2d.net/main/prog/c/image_io-01.html
    def read_pgm(pgmfilename):
        # cv2.imread(pgmfilename, -1)
        """Return a raster of integers from a PGM as a list of lists."""
        pgmf = open(pgmfilename, 'rb')
        # P1 to P6
        assert pgmf.readline() == 'P5\n'
        (width, height) = [int(i) for i in pgmf.readline().split()]
        depth = int(pgmf.readline())
        assert depth <= 255

        raster = []
        for y in range(height):
            row = []
            for y in range(width):
                row.append(ord(pgmf.read(1)))
            raster.append(row)
        return raster


if __name__ == '__main__':
    # All
    # d = NYU_V2Dataset()
    # Parts
    # d = NYU_V2Dataset(part_name='basements')
    d = NYU_V2Dataset(part_name='bathrooms_part1')
    # d = NYU_V2Dataset(part_name='bathrooms_part4')
    # d = NYU_V2Dataset(part_name='bedrooms_part1')
    # d = NYU_V2Dataset(part_name='bedrooms_part7')
    # d = NYU_V2Dataset(part_name='bookstore_part1')
    # d = NYU_V2Dataset(part_name='bookstore_part3')
    # d = NYU_V2Dataset(part_name='cafe')
    # d = NYU_V2Dataset(part_name='classrooms')
    # d = NYU_V2Dataset(part_name='dining_rooms_part1')
    # d = NYU_V2Dataset(part_name='dining_rooms_part2')
    # d = NYU_V2Dataset(part_name='furniture_stores')
    # d = NYU_V2Dataset(part_name='home_offices')
    # d = NYU_V2Dataset(part_name='kitchens_part1')
    # d = NYU_V2Dataset(part_name='kitchens_part3')
    # d = NYU_V2Dataset(part_name='libraries')
    # d = NYU_V2Dataset(part_name='living_rooms_part1')
    # d = NYU_V2Dataset(part_name='living_rooms_part4')
    # d = NYU_V2Dataset(part_name='misc_part1')
    # d = NYU_V2Dataset(part_name='misc_part2')
    # d = NYU_V2Dataset(part_name='offices_part1')
    # d = NYU_V2Dataset(part_name='offices_part2')
    # d = NYU_V2Dataset(part_name='office_kitchens')
    # d = NYU_V2Dataset(part_name='playrooms')
    # d = NYU_V2Dataset(part_name='reception_rooms')
    # d = NYU_V2Dataset(part_name='studies')
    # d = NYU_V2Dataset(part_name='study_rooms')

    print(len(d))
    img = d[0]
    # img, label = d[0]
    # (3, 375, 1242)
    print(img)
    print(img.shape)
    # print(img.debug_print())
    # point = d[0]
    # print(point.debug_print())

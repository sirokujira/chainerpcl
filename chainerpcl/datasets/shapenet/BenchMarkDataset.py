import chainer
import os
from PIL import Image
import numpy as np
import random
from chainer import Variable

from chainerpcl.chainer_experimental.datasets.sliceable import GetterDataset

root = 'pfnet/chainerpcl/PrincetonMODEL'
url_data_10 = 'http://vision.princeton.edu/projects/2014/3DShapeNets/ModelNet10.zip'
url_data_40 = 'http://modelnet.cs.princeton.edu/ModelNet40.zip'


PrincetonMODEL_label_10_names = (
    'Sky',
    'Building',
    'Pole',
    'Road',
    'Pavement',
    'Tree',
    'SignSymbol',
    'Fence',
    'Car',
    'Pedestrian',
    'Bicyclist',
)

PrincetonMODEL_label_10_colors = (
    (128, 128, 128),
    (128, 0, 0),
    (192, 192, 128),
    (128, 64, 128),
    (60, 40, 222),
    (128, 128, 0),
    (192, 128, 128),
    (64, 64, 128),
    (64, 0, 128),
    (64, 64, 0),
    (0, 128, 192),
)

PrincetonMODEL_label_40_names = (
    'airplane',
    'bathtub',
    'bed',
    'bench',
    'bookshelf',
    'bottle',
    'bowl',
    'car',
    'chair',
    'cone',
    'cup',
    'curtain',
    'desk',
    'door',
    'dresser',
    'flower_pot',
    'glass_box',
    'guitar',
    'keyboard',
    'lamp',
    'laptop',
    'mantel',
    'monitor',
    'night_stand',
    'person',
    'piano',
    'plant',
    'radio',
    'range_hood',
    'sink',
    'sofa',
    'stairs',
    'stool',
    'table',
    'tent',
    'toilet',
    'tv_stand',
    'vase',
    'wardrobe',
    'xbox',
)

PrincetonMODEL_label_40_colors = (
    (128, 128, 128),
    (128, 0, 0),
    (192, 192, 128),
    (128, 64, 128),
    (60, 40, 222),
    (128, 128, 0),
    (192, 128, 128),
    (64, 64, 128),
    (64, 0, 128),
    (64, 64, 0),
    (0, 128, 192),
    (128, 128, 128),
    (128, 0, 0),
    (192, 192, 128),
    (128, 64, 128),
    (60, 40, 222),
    (128, 128, 0),
    (192, 128, 128),
    (64, 64, 128),
    (64, 0, 128),
    (64, 64, 0),
    (0, 128, 192),
    (128, 128, 128),
    (128, 0, 0),
    (192, 192, 128),
    (128, 64, 128),
    (60, 40, 222),
    (128, 128, 0),
    (192, 128, 128),
    (64, 64, 128),
    (64, 0, 128),
    (64, 64, 0),
    (0, 128, 192),
    (128, 128, 128),
    (128, 0, 0),
    (192, 192, 128),
    (128, 64, 128),
    (60, 40, 222),
    (128, 128, 0),
    (192, 128, 128),
    (64, 64, 128),
    (64, 0, 128),
    (64, 64, 0),
    (0, 128, 192),
)

PrincetonMODEL_ignore_label_color = (0, 0, 0)


# class BenchMarkDataset(GetterDataset):
class BenchMarkDataset(chainer.dataset.DatasetMixin):
    r"""Image dataset for test split of `KITTI dataset`_.

    .. _`KITTI dataset`: http://www.cvlibs.net/datasets/kitti/raw_data.php

    .. note::

    Args:
        data_dir (string): Path to the dataset directory. The directory should
            contain the :obj:`---` directory. If this is
            :obj:`auto`, this class will automatically download data for you
            under :obj:`$CHAINER_DATASET_ROOT/pfnet/chainerpcl/kitti`.
        date ({'2011_09_26', '2011_09_28', '2011_09_29',
                                           '2011_09_30', '2011_10_03'}):
            reference Calibration datas.
        drive_num ({'0xxx'}): get datas drive No.
        color (bool): use glay/color image.
        sync (bool): get timer sync/nosync data.
        is_left (bool): left/right camera image use 2type.
        tracklet (bool): 3d bblox data. date only 2011_09_26.

    This dataset returns the following data.

    .. csv-table::
        :header: name, shape, dtype, format

        :obj:`img`, ":math:`(3, H, W)`", :obj:`float32`, \
        "RGB, :math:`[0, 255]`"
        :obj:`bbox` [#kitti_bbox_1]_, ":math:`(R, 4)`", :obj:`float32`, \
        ":math:`(y_{min}, x_{min}, y_{max}, x_{max})`"
        :obj:`label`, scalar, :obj:`int32`, ":math:`[0, \#class - 1]`"
    """
    def __init__(self, root, npoints = 2500, classification = False, class_choice = None, train = True):
        self.npoints = npoints
        self.root = root
        self.catfile = os.path.join(self.root, 'synsetoffset2category.txt')
        self.cat = {}

        self.classification = classification

        with open(self.catfile, 'r') as f:
            for line in f:
                ls = line.strip().split()
                self.cat[ls[0]] = ls[1]
        #print(self.cat)
        if not class_choice is  None:
            self.cat = {k:v for k,v in self.cat.items() if k in class_choice}

        self.meta = {}
        for item in self.cat:
            #print('category', item)
            self.meta[item] = []
            dir_point = os.path.join(self.root, self.cat[item], 'points')
            dir_seg = os.path.join(self.root, self.cat[item], 'points_label')
            #print(dir_point, dir_seg)
            fns = sorted(os.listdir(dir_point))
            if train:
                fns = fns[:int(len(fns) * 0.9)]
            else:
                fns = fns[int(len(fns) * 0.9):]

            #print(os.path.basename(fns))
            for fn in fns:
                token = (os.path.splitext(os.path.basename(fn))[0])
                self.meta[item].append((os.path.join(dir_point, token + '.pts'), os.path.join(dir_seg, token + '.seg')))

        self.datapath = []
        for item in self.cat:
            for fn in self.meta[item]:
                self.datapath.append((item, fn[0], fn[1]))


        self.classes = dict(zip(self.cat, range(len(self.cat))))
        print(self.classes)
        self.num_seg_classes = 0
        if not self.classification:
            for i in range(len(self.datapath)//50):
                l = len(np.unique(np.loadtxt(self.datapath[i][-1]).astype(np.uint8)))
                if l > self.num_seg_classes:
                    self.num_seg_classes = l
        #print(self.num_seg_classes)


    def get_PrincetonMODEL_10():
        data_root = download.get_dataset_directory(root)
        download_file_path = utils.cached_download(url)
        utils.extractall(download_file_path, data_root, os.path.splitext(url)[1])
        return data_root


    def get_PrincetonMODEL_40(name):
        data_root = download.get_dataset_directory(root)
        download_file_path = utils.cached_download(url)
        utils.extractall(download_file_path, data_root, os.path.splitext(url)[1])


    def get_PrincetonMODEL_ALL(name):
        data_root = download.get_dataset_directory(root)
        download_file_path = utils.cached_download(url)
        utils.extractall(download_file_path, data_root, os.path.splitext(url)[1])


    def __getitem__(self, index):
        fn = self.datapath[index]
        cls = self.classes[self.datapath[index][0]]
        point_set = np.loadtxt(fn[1]).astype(np.float32)
        # seg = np.loadtxt(fn[2]).astype(np.int64)
        # Chainer
        seg = np.loadtxt(fn[2]).astype(np.int32)
        # print(point_set.shape, seg.shape)

        choice = np.random.choice(len(seg), self.npoints, replace=True)
        #resample
        point_set = point_set[choice, :]
        seg = seg[choice]
        # numpy convert torch array(Tensor)?
        # point_set = torch.from_numpy(point_set)
        # seg = torch.from_numpy(seg)
        # cls = torch.from_numpy(np.array([cls]).astype(np.int64))
        # Chainer
        point_set = Variable(point_set)
        seg = Variable(seg)
        cls = Variable(np.array([cls]).astype(np.int32))

        if self.classification:
            return point_set, cls
        else:
            return point_set, seg

    def __len__(self):
        return len(self.datapath)


if __name__ == '__main__':
    print('test')
    d = BenchMarkDataset(root = 'shapenetcore_partanno_segmentation_benchmark_v0', class_choice = ['Chair'])
    print(len(d))
    ps, seg = d[0]
    # ng 
    # print(ps.size(), ps.type(), seg.size(),seg.type())
    # print(str(ps.shape()), str(ps.dtype()), str(seg.shape()), str(seg.dtype()))
    print(str(ps.debug_print()), str(seg.debug_print()))

    d = BenchMarkDataset(root = 'shapenetcore_partanno_segmentation_benchmark_v0', classification = True)
    print(len(d))
    ps, cls = d[0]
    # print(ps.size(), ps.type(), cls.size(),cls.type())
    # print(ps, cls)
    print(str(ps.debug_print()), str(cls.debug_print()))

import os
try:
    # 3.x
    from urllib.parse import urljoin
except ImportError:
    # 2.7
    from urlparse import urljoin

from chainer.dataset import download

from chainerpcl import utils

import numpy as np

url_base = 'https://pointcloud.pref.shizuoka.jp/lasmap/'
url_target_base = 'https://pointcloud.pref.shizuoka.jp/lasmap/ankendetail?ankenno='


def get_ShizuokaDB_Data(self, root, construction_number):
    import shutil
    construction_number_repalce_hyphen = construction_number.replace('-', '')

    # las file urllink
    # construction_number_values = construction_number_dictionary[construction_number]
    # print(construction_number_values)

    # import urllib3
    import bs4
    import urllib.request
    import re
    target_url = url_target_base + construction_number_repalce_hyphen
    print(target_url)
    html = urllib.request.urlopen(target_url)
    soup = bs4.BeautifulSoup(html, "html.parser")

    construction_number_values = list()
    # for link in soup.findAll("a"):
    for link in soup.findAll("a", text=re.compile("las")):
        if "las" in soup.a.get("href"):
            # print(link.get('href'))
            construction_number_values.append(link.get('href'))

    print(construction_number_values)

    construction_root = os.path.join(root, construction_number_repalce_hyphen)

    # data_root = download.get_dataset_directory(root)
    data_root = download.get_dataset_directory(construction_root)
    print('dst path : ' + data_root)

    if not os.path.exists(data_root):
        os.mkdir(data_root)

    for cnv in construction_number_values:
        # url_data = urljoin(url_base, 'public' + '/' + construction_number_repalce_hyphen + '/' + cnv)
        url_data = urljoin(url_base, cnv)
        print('url : ' + url_data)

        download_file_path = utils.cached_download(url_data)
        print('filepath : ' + download_file_path)
        # download las file
        ext = os.path.splitext(url_data)[1]
        # utils.extractall(download_file_path, data_root, ext)
        # os.rename(download_file_path, os.path.join(data_root, cnv))
        # shutil.move(download_file_path, os.path.join(data_root, cnv))
        shutil.copy(download_file_path, os.path.join(data_root, os.path.basename(cnv)))

    return data_root



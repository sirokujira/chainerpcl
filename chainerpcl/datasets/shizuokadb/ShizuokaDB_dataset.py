import os
import warnings

import numpy as np

from chainer.dataset import download
from chainerpcl import utils
from chainerpcl.utils.point import read_las_points
from chainerpcl.utils.point import read_las_points_to_pcl

from chainerpcl.chainer_experimental.datasets.sliceable import GetterDataset
from chainerpcl.datasets.shizuokadb.shizuokadb_utils import get_ShizuokaDB_Data


def _check_available():
    if not _available:
        warnings.warn(
            'laspy is not installed in your environment,'
            'so the dataset cannot be loaded.'
            'Please install laspy to load dataset.\n\n'
            '$ pip install laspy>=0.3.0')


class ShizuokaDBDataset(GetterDataset):
    r"""PointCloud dataset for test split of `ShizuokaDB dataset`_.

    .. _`ShizuokaDB dataset`: https://pointcloud.pref.shizuoka.jp/

    .. note::

    Args:
        data_dir (string): Path to the dataset directory. The directory should
            contain the :obj:`---` directory. If :obj:`auto` is given,
            it uses :obj:`$CHAINER_DATSET_ROOT/pfnet/chainerpcl/ShizuokaDB` by
            default.
        construction_number ({'28-XXX00-03-00-04',  '28-W0608-01-11-01', ...}):
            reference Calibration datas.
    This dataset returns the following data.
        :header: name, shape, dtype, format

        :obj:`point`, ":math:`(3, H, W)`", :obj:`float32`, \
        "RGB, :math:`[0, 255]`"
    """

    def __init__(self, data_dir='auto', construction_number=''):
        super(KITTIBboxDataset, self).__init__()

        _check_available()

        if data_dir == 'auto':
            data_dir = get_ShizuokaDB_Data(os.path.join('pfnet', 'chainerpcl', 'ShizuokaDB'), construction_number)

        # point
        data_dir = self.get_ShizuokaDB_Point(data_dir, construction_number)


    def __getitem__(self, index):
        # point_set = read_las_points(self.point_paths[index])
        point_set = read_las_points_to_pcl(self.point_paths[index])
        return point_set


    def __len__(self):
        return len(self.point_paths)


    # point
    def get_example(self, i):
        """Returns the i-th test point.

        Returns a color point. The color point is in XYZRGB format.

        Args:
            i (int): The index of the example.

        Returns:
            A color point whose shape is (6, x, y, z). H and W are height and
            width of the point.
            The dtype of the color point is :obj:`numpy.float32`.

        """
        # point_set = read_las_points(self.point_paths[i])
        point_set = read_las_points_to_pcl(self.point_paths[i])
        return point_set


    def get_ShizuokaDB_Data(self, root, construction_number):
        import shutil
        construction_number_repalce_hyphen = construction_number.replace('-', '')

        # las file urllink
        # construction_number_values = construction_number_dictionary[construction_number]
        # print(construction_number_values)

        # import urllib3
        import bs4
        import urllib.request
        import re
        target_url = url_target_base + construction_number_repalce_hyphen
        print(target_url)
        html = urllib.request.urlopen(target_url)
        soup = bs4.BeautifulSoup(html, "html.parser")

        construction_number_values = list()
        # for link in soup.findAll("a"):
        for link in soup.findAll("a", text=re.compile("las")):
            if "las" in soup.a.get("href"):
                # print(link.get('href'))
                construction_number_values.append(link.get('href'))

        print(construction_number_values)

        construction_root = os.path.join(root, construction_number_repalce_hyphen)

        # data_root = download.get_dataset_directory(root)
        data_root = download.get_dataset_directory(construction_root)
        print('dst path : ' + data_root)

        if not os.path.exists(data_root):
            os.mkdir(data_root)

        for cnv in construction_number_values:
            # url_data = urljoin(url_base, 'public' + '/' + construction_number_repalce_hyphen + '/' + cnv)
            url_data = urljoin(url_base, cnv)
            print('url : ' + url_data)

            download_file_path = utils.cached_download(url_data)
            print('filepath : ' + download_file_path)
            # download las file
            ext = os.path.splitext(url_data)[1]
            # utils.extractall(download_file_path, data_root, ext)
            # os.rename(download_file_path, os.path.join(data_root, cnv))
            # shutil.move(download_file_path, os.path.join(data_root, cnv))
            shutil.copy(download_file_path, os.path.join(data_root, os.path.basename(cnv)))

        return data_root


    def get_ShizuokaDB_Point(self, data_root, construction_number):
        point_dir = data_root
        print(point_dir)

        self.point_paths = list()
        if not os.path.exists(point_dir):
            raise ValueError(
                'ShizuokaDB dataset does not exist at the expected location.'
                'Please download it from https://pointcloud.pref.shizuoka.jp/'
                'Then place directory image at {}.'.format(point_dir))

        for point_path in sorted(glob.glob(os.path.join(point_dir, '*.las'))):
                self.point_paths.append(point_path)

        return data_root


if __name__ == '__main__':
    # https://pointcloud.pref.shizuoka.jp/lasmap/ankendetail?ankenno=28XXX00030004
    # d = ShizuokaDBDataset(construction_number='28-XXX00-03-00-04')
    d = ShizuokaDBDataset(construction_number='28-W0608-01-11-01')
    # d = ShizuokaDBDataset(construction_number='29-C2001-01-13-35')
    # d = ShizuokaDBDataset(construction_number='29-C2001-01-13-05')
    # d = ShizuokaDBDataset(construction_number='28-K2460-01-11-02')
    # d = ShizuokaDBDataset(construction_number='29-C2001-01-13-54')
    # d = ShizuokaDBDataset(construction_number='29-C2001-01-13-60')

    # las file path
    # https://pointcloud.pref.shizuoka.jp/lasmap/public/29C2001011335/02_Laser3D_Color_I_0000_C12-L1.las

    # point
    print(len(d))
    point = d.get_example(0)
    # All
    for i in range(1, len(d)):
        tmpPoint = d.get_example(i)
        point = np.concatenate((point, tmpPoint), axis=0)

    print(point)
    print(point.shape)
    # print(point.debug_print())
    # point2 = d.get_example(1)

    # viewer?
    import pcl
    from pcl import pcl_visualization
    # p = pcl.PointCloud()
    # NG
    # p = pcl.PointCloud_PointXYZRGB()
    # OK
    p = pcl.PointCloud_PointXYZRGBA()
    # print(point[:, 0:3])
    # print(np.concatenate([np.mean(point, 0)[0:3], np.zeros(3)]))
    # XYZ_RGB(1)
    mean_param = np.concatenate([np.mean(point, 0)[0:3], np.zeros(1)])
    # XYZRGB
    # mean_param = np.concatenate([np.mean(point, 0)[0:3], np.zeros(3)])
    # ptcloud_centred = point - mean_param
    # ptcloud_centred = point
    # ptcloud_centred = np.concatenate((point, point2, point3), axis=0) - mean_param
    ptcloud_centred = point - mean_param
    print(ptcloud_centred)
    p.from_array(np.array(ptcloud_centred, dtype=np.float32))

    ## Visualization
    visual = pcl_visualization.CloudViewing()
    # visual.ShowMonochromeCloud(p, b'cloud')
    # visual.ShowColorCloud(p, b'cloud')
    visual.ShowColorACloud(p, b'cloud')

    v = True
    while v:
        v=not(visual.WasStopped())

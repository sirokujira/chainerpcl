import glob
import os
import shutil

import numpy as np

import chainer
from chainer.dataset import download
from chainerpcl import utils


# root = 'pfnet/chainerpcl/S3DIS'
url = 'http://buildingparser.stanford.edu/dataset.html#Download'

# meta/class_names.txt
S3DIS_label_names = (
    'ceiling',
    'floor',
    'wall',
    'beam',
    'column',
    'window',
    'door',
    'table',
    'chair',
    'sofa',
    'bookcase',
    'board',
    'clutter',
)

S3DIS_label_colors = (
    (128, 128, 128),
    (128, 0, 0),
    (192, 192, 128),
    (128, 64, 128),
    (60, 40, 222),
    (128, 128, 0),
    (192, 128, 128),
    (64, 64, 128),
    (64, 0, 128),
    (64, 64, 0),
    (192, 128, 0),
    (192, 128, 128),
    (192, 0, 192),
)
S3DIS_ignore_label_color = (0, 0, 0)


class S3DISDataset(chainer.dataset.DatasetMixin):

    """Dataset class for a semantic segmantion task on Pointnet `u`_.

    .. _`u`: http://buildingparser.stanford.edu/dataset.html

    Args:
        data_dir (string): Path to the root of the training data. If this is
            :obj:`auto`, this class will automatically download data for you
            under :obj:`$CHAINER_DATASET_ROOT/pfnet/chainerpcl/S3DIS`.
        split ({'train', 'val', 'test'}): Select from dataset splits used
            in Pointnet Dataset.

    """

    def __init__(self, data_dir='auto', AreaNum='6'):
        if data_dir == 'auto':
            # need Registry and url path is Google Drive
            data_dir = get_S3DIS_Data()

        self.get_S3DIS_Point(data_dir, AreaNum)


    def get_S3DIS_Data(self):
        data_root = download.get_dataset_directory(root)
        download_file_path = utils.cached_download(url)
        utils.extractall(download_file_path, data_root, os.path.splitext(url)[1])
        return data_root


    def get_S3DIS_Point(self, data_root, AreaNum):
        # point_dir = os.path.join(data_root, 'Area_' + AreaNum)
        point_dir = os.path.join(data_root)

        self.point_paths = list()

        for point_path in sorted(glob.glob(os.path.join(point_dir, '*.npy'))):
            self.point_paths.append(point_path)


    def __len__(self):
        return len(self.point_paths)


    def get_example(self, i):
        """Returns the i-th example.

        Returns a color image and a label image. The color image is in CHW
        format and the label image is in HW format.

        Args:
            i (int): The index of the example.

        Returns:
            tuple of a color image and a label whose shapes are (3, H, W) and
            (H, W) respectively. H and W are height and width of the image.
            The dtype of the color image is :obj:`numpy.float32` and
            the dtype of the label image is :obj:`numpy.int32`.

        """
        # print(self.point_paths[i])
        return np.load(self.point_paths[i])


if __name__ == '__main__':
    # Download archive file google Drive(need Authentication)
    # d = S3DISDataset()
    # d = S3DISDataset(data_dir='E:/chainerpcl/stanford_indoor3d/Area_6', AreaNum='6')
    # d = S3DISDataset(data_dir='E:/chainerpcl/stanford_indoor3d/Area_5', AreaNum='6')
    # d = S3DISDataset(data_dir='E:/chainerpcl/stanford_indoor3d/Area_4', AreaNum='6')
    # d = S3DISDataset(data_dir='E:/chainerpcl/stanford_indoor3d/Area_3', AreaNum='6')
    d = S3DISDataset(data_dir='E:/chainerpcl/stanford_indoor3d/Area_2', AreaNum='6')
    # d = S3DISDataset(data_dir='E:/chainerpcl/stanford_indoor3d/Area_1', AreaNum='6')

    # point
    point = d.get_example(0)
    # All
    for i in range(1, len(d)):
        tmpPoint = d.get_example(i)
        point = np.concatenate((point, tmpPoint), axis=0)

    print(point)
    print(point.shape)

    # print(point[:, 0:3])
    red   = point[:, 3].astype(np.uint32)
    green = point[:, 4].astype(np.uint32)
    blue  = point[:, 5].astype(np.uint32)
    rgb = np.left_shift(red, 16) + np.left_shift(green, 8) + np.left_shift(blue, 0)
    ptcloud = np.vstack((point[:, 0], point[:, 1], point[:, 2], rgb)).transpose()

    label_idxs = point[:, 6].astype(np.uint8)
    # print(label_idxs)
    label_red = list()
    label_green = list()
    label_blue = list()
    for label_idx in label_idxs:
        tmp_red, tmp_green, tmp_blue =  S3DIS_label_colors[label_idx]
        label_red.append(tmp_red)
        label_green.append(tmp_green)
        label_blue.append(tmp_blue)

    label_red   = np.array(label_red)
    label_green = np.array(label_green)
    label_blue  = np.array(label_blue)
    # print(label_red)
    # print(label_green)
    # print(label_blue)
    # label_red, label_green, label_blue = S3DIS_label_colors(label_id)
    # label_red   = label_red.astype(np.uint32)
    # label_green = label_green.astype(np.uint32)
    # label_blue  = label_blue.astype(np.uint32)
    label_rgb = np.left_shift(label_red, 16) + np.left_shift(label_green, 8) + np.left_shift(label_blue, 0)
    pt_cloud_label = np.vstack((point[:, 0], point[:, 1], point[:, 2], label_rgb)).transpose()

    import pcl
    from pcl import pcl_visualization
    p = pcl.PointCloud_PointXYZRGBA()

    ## Visualization
    visual = pcl_visualization.CloudViewing()

    mean_param = np.zeros(4)
    # mean_param = np.concatenate([np.mean(ptcloud, 0)[0:3], np.zeros(1)])

    # raw
    ptcloud_centred = ptcloud - mean_param
    # print(ptcloud_centred)
    p.from_array(np.array(ptcloud_centred, dtype=np.float32))

    # label
    # p2 = pcl.PointCloud_PointXYZRGBA()
    # ptcloud_centred_label = pt_cloud_label - mean_param
    # p2.from_array(np.array(ptcloud_centred_label, dtype=np.float32))

    visual.ShowColorACloud(p, b'cloud')
    # visual.ShowColorACloud(p2, b'cloud_label')

    flag = True
    while flag:
        flag != visual.WasStopped()
    end

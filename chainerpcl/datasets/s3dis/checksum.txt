S3DIS Dataset
http://buildingparser.stanford.edu/dataset.html

Checksum
https://github.com/alexsax/2D-3D-Semantics/wiki/Checksum-Values-for-Data

Checksums

No XYZ

File Name

MD5 checksum
area_1_no_xyz.tar 21098fbe93b561e30e79197a95fa4fd2 
area_2_no_xyz.tar d630696e4588c2329483bc07210d0ba9 
area_3_no_xyz.tar 09561f6aa6a87f9ae7d1beb3973964fb 
area_4_no_xyz.tar 6798c3196ae1e9e4d25073c53aec1ad5 
area_5a_no_xyz.tar 961a490072093fca726a483b74169b48 
area_5b_no_xyz.tar 85fd6de1a7c7247d8563ed5976dbd06d 
area_6_no_xyz.tar 71543fc18f2f444fd17243e68f064ff0 

With XYZ

File Name

MD5 checksum
area_1.tar.gz e07f142e421f1e106742de3a04bf9275 
area_2.tar.gz 3670f6777a370b3935829376718978a8 
area_3.tar.gz 3920a5c3a763bec6e9dd83bf18a565e9 
area_4.tar.gz 47efe992496d83ae32270e448c63ad8c 
area_5a.tar.gz 324aba1e8a53b3c75a0b394ef71bfcb4 
area_5b.tar.gz f78f526f0c573b05644abebf4b4871ce 
area_6.tar.gz 63d5782c8e12afaddec7f69be69267c2 


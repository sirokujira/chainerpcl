import numpy as np


def read_points(path, dtype=np.float32, color=True):
    """Read an point from a file.

    This function reads an point from given file. The point is xyz format and
    the range of its value is :math:`[0, 255]`. If :obj:`color = True`, the
    order of the channels is RGB.

    Args:
        path (str): A path of point file.
        dtype: The type of array. The default value is :obj:`~numpy.float32`.
        color (bool): This option determines the number of channels.
            If :obj:`True`, the number of channels is three. In this case,
            the order of the channels is RGB. This is the default behaviour.
            If :obj:`False`, this function returns a grayscale image.

    Returns:
        ~numpy.ndarray: An point.
    """
    try:
        import pcl
    except:
        print("Cannot import pcl")
        return

    # f = pcl.loadXYZRGB(path)
    f = pcl.load(path)
    try:
        # Convert numpy
        point = f.to_array()
        point = np.asarray(point, dtype=dtype)
        print(point.shape)
        # point = point.reshape((2500, 3))
        point = np.resize(point, (2500, 3))
    finally:
        if hasattr(f, 'close'):
            f.close()

    if point.ndim == 2:
        # reshape [X, Y, Z][:] -> [xArray, yArray, zArray]
        # return point[np.newaxis]
        # return point.transpose()
        return point.transpose()[np.newaxis]
    else:
        return point


def read_pcd_points(path, dtype=np.float32, color=True):
    """Read an point from a file.

    This function reads an point from given file. The point is xyz format and
    the range of its value is :math:`[0, 255]`. If :obj:`color = True`, the
    order of the channels is RGB.

    Args:
        path (str): A path of point file.
        dtype: The type of array. The default value is :obj:`~numpy.float32`.
        color (bool): This option determines the number of channels.
            If :obj:`True`, the number of channels is three. In this case,
            the order of the channels is RGB. This is the default behaviour.
            If :obj:`False`, this function returns a grayscale image.

    Returns:
        ~numpy.ndarray: An point.
    """
    try:
        import pcl
    except:
        print("Cannot import pcl")
        return

    # f = pcl.loadXYZRGB(path)
    f = pcl.load(path)
    try:
        # Convert numpy
        point = f.to_array()
        point = np.asarray(point, dtype=dtype)
        print(point.shape)
        # point = point.reshape((2500, 3))
        point = np.resize(point, (2500, 3))
    finally:
        if hasattr(f, 'close'):
            f.close()

    if point.ndim == 2:
        # reshape [X, Y, Z][:] -> [xArray, yArray, zArray]
        # return point[np.newaxis]
        # return point.transpose()
        return point.transpose()[np.newaxis]
    else:
        return point


def read_las_points(path, dtype=np.float32, setPoint=True, color=True):
    import random
    import scipy as sc
    # laspy librairy, read las file
    try:
        from laspy import file
    except:
        print("Cannot import laspy.file")
        return

    f = file.File(path, mode='r')
    # Store pointcloud in array
    if color == True:
        ptcloud = np.vstack((f.x, f.y, f.z, f.red, f.green, f.blue)).transpose()
    else:
        ptcloud = np.vstack((f.x, f.y, f.z)).transpose()

    f.close()

    # ptcloud = np.array(ptcloud, dtype=np.float32)
    return ptcloud

    # # Centred the data
    # ptcloud_centred = ptcloud - np.mean(ptcloud,0)
    # 
    # # Simulate an intensity information between 0 and 1
    # ptcloud_centred = sc.append(ptcloud_centred, np.zeros((ptcloud.shape[0], 1)),axis=1) # Ajout d'une ligne (axis=0)
    # for i in range(ptcloud_centred.shape[0] - 1):
    #     ptcloud_centred[i, 3] = random.random()
    # 
    # # ptcloud_centred = np.array(ptcloud_centred, dtype=np.float32)
    # return ptcloud_centred
    # 
    # # p = pcl.PointCloud_PointXYZI()
    # # p.from_array(np.array(ptcloud_centred, dtype=np.float32))


def read_las_points_to_pcl(path, dtype=np.float32, setPoint=True, color=True):
    import random
    import scipy as sc
    # laspy librairy, read las file
    try:
        from laspy import file
    except:
        print("Cannot import laspy.file")
        return

    f = file.File(path, mode='r')
    # check las file version
    print('laspy headers info: ')
    print('version : ' + str(f._header.version))
    print('data_format_id : ' + str(f._header.data_format_id))
    # RGB あり(pcl 側は RGBA で扱うこと[RGB がうまくいかないため])
    if f._header.data_format_id in (2, 3, 5):
        red = (f.red)
        green = (f.green)
        blue = (f.blue)
        print(str(red))
        print(str(green))
        print(str(blue))
        # red = (f.red).astype(np.uint8)
        # green = (f.green).astype(np.uint8)
        # blue = (f.blue).astype(np.uint8)
        # red =   np.right_shift(np.left_shift(red, 8), 8).astype(np.uint8)
        # green = np.right_shift(np.left_shift(green, 8), 8).astype(np.uint8)
        # blue =  np.right_shift(np.left_shift(blue, 8), 8).astype(np.uint8)
        # red =   np.left_shift(np.right_shift(red, 8), 8).astype(np.uint8)
        # green = np.left_shift(np.right_shift(green, 8), 8).astype(np.uint8)
        # blue =  np.left_shift(np.right_shift(blue, 8), 8).astype(np.uint8)
        # ShizuokaDB のデータで 16bit のうち 先頭8bit にデータが入っているケースに遭遇したため、以下の対応を入れている
        red = np.right_shift(red, 8).astype(np.uint8)
        green = np.right_shift(green, 8).astype(np.uint8)
        blue = np.right_shift(blue, 8).astype(np.uint8)
        # print(str(red))
        # print(str(green))
        # print(str(blue))

    # print('version : ' + str(f.header.version))
    # print('data_format_id : ' + str(f.header.data_format_id))
    # Store pointcloud in array
    if color == True:
        print('test(type:uint8)')
        print(red)
        print(green)
        print(blue)
        red = red.astype(np.uint32)
        green = green.astype(np.uint32)
        blue = blue.astype(np.uint32)
        print('test(type:uint32)')
        print(red)
        print(green)
        print(blue)
        rgb = np.left_shift(red, 16) + np.left_shift(green, 8) + np.left_shift(blue, 0)
        # ptcloud = np.vstack((f.x, f.y, f.z, red + green + blue)).transpose()
        ptcloud = np.vstack((f.x, f.y, f.z, rgb)).transpose()
    else:
        ptcloud = np.vstack((f.x, f.y, f.z)).transpose()

    f.close()

    # ptcloud = np.array(ptcloud, dtype=np.float32)
    return ptcloud

    # # Centred the data
    # ptcloud_centred = ptcloud - np.mean(ptcloud,0)
    # 
    # # Simulate an intensity information between 0 and 1
    # ptcloud_centred = sc.append(ptcloud_centred, np.zeros((ptcloud.shape[0], 1)),axis=1) # Ajout d'une ligne (axis=0)
    # for i in range(ptcloud_centred.shape[0] - 1):
    #     ptcloud_centred[i, 3] = random.random()
    # 
    # # ptcloud_centred = np.array(ptcloud_centred, dtype=np.float32)
    # return ptcloud_centred
    # 
    # # p = pcl.PointCloud_PointXYZI()
    # # p.from_array(np.array(ptcloud_centred, dtype=np.float32))

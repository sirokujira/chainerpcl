from chainer import cuda


def bbox_3d_iou(bbox_3d_a, bbox_3d_b):
    """Calculate the Intersection of Unions (IoUs) between bounding 3D boxes.

    IoU is calculated as a ratio of area of the intersection
    and area of the union.

    This function accepts both :obj:`numpy.ndarray` and :obj:`cupy.ndarray` as
    inputs. Please note that both :obj:`bbox_3d_a` and :obj:`bbox_3d_b` need to be
    same type.
    The output is same type as the type of the inputs.

    Args:
        bbox_3d_a (array): An array whose shape is :math:`(N, 6 [dim])`.
            :math:`N` is the number of bounding 3D boxes.
            The dtype should be :obj:`numpy.float32`.
        bbox_3d_b (array): An array similar to :obj:`bbox_3d_a`,
            whose shape is :math:`(K, 6 [dim])`.
            The dtype should be :obj:`numpy.float32`.

    Returns:
        array:
        An array whose shape is :math:`(N, K)`. \
        An element at index :math:`(n, k)` contains IoUs between \
        :math:`n` th bounding 3D box in :obj:`bbox_3d_a` and :math:`k` th bounding \
        box in :obj:`bbox_3d_b`.

    """
    if bbox_3d_a.shape[1] != 6 or bbox_3d_b.shape[1] != 6:
        raise IndexError
    xp = cuda.get_array_module(bbox_3d_a)

    # top left
    tl = xp.maximum(bbox_3d_a[:, None, :3], bbox_3d_b[:, :3])
    # bottom right
    br = xp.minimum(bbox_3d_a[:, None, 3:], bbox_3d_b[:, 3:])

    area_i = xp.prod(br - tl, axis=3) * (tl < br).all(axis=3)
    area_a = xp.prod(bbox_3d_a[:, 3:] - bbox_3d_a[:, :3], axis=1)
    area_b = xp.prod(bbox_3d_b[:, 3:] - bbox_3d_b[:, :3], axis=1)
    return area_i / (area_a[:, None] + area_b - area_i)

from chainerpcl.visualizations.vis_pointcloud import vis_pointcloud
from chainerpcl.visualizations.vis_pointcloud import vis_pointcloud_pcl


def vis_bbox3d(points, bbox3d, label=None, score=None, label_names=None, ax=None):
    """Visualize bounding boxes inside pointcloud.

    Example:
            >>> from chainerpcl.datasets.KITTI import KITTI_dataset
            >>> from chainerpcl.datasets.KITTI.KITTI_dataset import KITTI_label_names
            >>> from chainerpcl.visualizations import vis_bbox3d
            >>> import matplotlib.pyplot as plt
            >>> from mpl_toolkits.mplot3d import Axes3D
            >>> dataset = KITTIDataset()
            >>> point, bbox3d, label = dataset[60]
            >>> vis_bbox_3d(point, bbox3d, label,
            ...         label_names=KITTIDataset_label_names)
            >>> plot.show()
    Args:
        points (~numpy.ndarray): An array of shape :math:`(3, height, width)`.
            This is in XYZ format and the range of its value is
            :math:`[0, 255]`.
        bbox3d (~numpy.ndarray): An array of shape :math:`(R, 6)`, where
            :math:`R` is the number of bounding boxes in the image.
            Each element is organized
            by :obj:`(y_min, x_min, y_max, x_max)` in the second axis.
        label (~numpy.ndarray): An integer array of shape :math:`(R,)`.
            The values correspond to id for label names stored in
            :obj:`label_names`. This is optional.
        score (~numpy.ndarray): A float array of shape :math:`(R,)`.
             Each value indicates how confident the prediction is.
             This is optional.
        label_names (iterable of strings): Name of labels ordered according
            to label ids. If this is :obj:`None`, labels will be skipped.
        ax (matplotlib.axes.Axis): The visualization is displayed on this
            axis. If this is :obj:`None` (default), a new axis is created.

    Returns:
        ~matploblib.axes.Axes:
        Returns the Axes object with the plot for further tweaking.

    """
    from matplotlib import pyplot as plt
    from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
    if label is not None and not len(bbox3d) == len(label):
        raise ValueError('The length of label must be same as that of bbox3d')
    if score is not None and not len(bbox3d) == len(score):
        raise ValueError('The length of score must be same as that of bbox3d')

    # Returns newly instantiated matplotlib.axes.Axes object if ax is None
    ax = vis_pointcloud(points)

    # If there is no bounding box to display, visualize the image and exit.
    if len(bbox3d) == 0:
        return ax

    for i, bb3d in enumerate(bbox3d):
        # voxel
        # list of sides' polygons of figure
        verts = [[bb3d[:,0], bb3d[:,1], bb3d[:,2], bb3d[:,3]],
                 [bb3d[:,4], bb3d[:,5], bb3d[:,6], bb3d[:,7]],
                 [bb3d[:,0], bb3d[:,1], bb3d[:,5], bb3d[:,4]],
                 [bb3d[:,2], bb3d[:,3], bb3d[:,7], bb3d[:,6]],
                 [bb3d[:,1], bb3d[:,2], bb3d[:,6], bb3d[:,5]],
                 [bb3d[:,4], bb3d[:,7], bb3d[:,3], bb3d[:,0]],
                 [bb3d[:,2], bb3d[:,3], bb3d[:,7], bb3d[:,6]]]

        # plot sides
        ax.add_collection3d(Poly3DCollection(verts, facecolors='cyan', linewidths=1, edgecolors='r', alpha=.25))

        caption = list()

        if label is not None and label_names is not None:
            lb = label[i]
            if not (0 <= lb < len(label_names)):
                raise ValueError('No corresponding name is given')
            caption.append(label_names[lb])
        if score is not None:
            sc = score[i]
            caption.append('{:.2f}'.format(sc))

        # if len(caption) > 0:
        #     ax.text(bb3d[:,1], bb3d[:,0],
        #             ': '.join(caption),
        #             style='italic',
        #             bbox3d={'facecolor': 'white', 'alpha': 0.7, 'pad': 10})

    # ax.set_xlabel('X')
    # ax.set_ylabel('Y')
    # ax.set_zlabel('Z')

    return ax


def vis_bbox3d_to_pcl(points, bbox3d, label=None, score=None, label_names=None, ax=None):
    """Visualize bounding boxes inside pointcloud.

    Example:
            >>> from chainerpcl.datasets.KITTI import KITTI_dataset
            >>> from chainerpcl.datasets.KITTI.KITTI_dataset import KITTI_label_names
            >>> from chainerpcl.visualizations import vis_bbox3d
            >>> import matplotlib.pyplot as plt
            >>> from mpl_toolkits.mplot3d import Axes3D
            >>> dataset = KITTIDataset()
            >>> point, bbox3d, label = dataset[60]
            >>> vis_bbox_3d(point, bbox3d, label,
            ...         label_names=KITTIDataset_label_names)
            >>> plot.show()
    Args:
        points (~numpy.ndarray): An array of shape :math:`(x, y, z)`.
            This is in XYZ format and the range of its value is
            :math:`[0, 255]`.
        bbox3d (~numpy.ndarray): An array of shape :math:`(R, 8)`, where
            :math:`R` is the number of bounding boxes in the point(x,y,z).
            Each element is organized
            by :obj:`(xyz1, xyz2, xyz3, xyz4, xyz5, xyz6, xyz7, xyz8)` in the second axis.
        label (~numpy.ndarray): An integer array of shape :math:`(R,)`.
            The values correspond to id for label names stored in
            :obj:`label_names`. This is optional.
        score (~numpy.ndarray): A float array of shape :math:`(R,)`.
             Each value indicates how confident the prediction is.
             This is optional.
        label_names (iterable of strings): Name of labels ordered according
            to label ids. If this is :obj:`None`, labels will be skipped.
        ax (matplotlib.axes.Axis): The visualization is displayed on this
            axis. If this is :obj:`None` (default), a new axis is created.

    Returns:
        ~matploblib.axes.Axes:
        Returns the Axes object with the plot for further tweaking.

    """
    pass
    
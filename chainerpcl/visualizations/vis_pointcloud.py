import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

def vis_pointcloud(points, ax=None):
    """Visualize a point cloud.

    Args:
        points (~numpy.ndarray): An array of shape :math:`(x, y, z)`.
            This is in  format PointCloud and the range of its value is
            :math:`[-∞, +∞]`. (float32?)
        ax (matplotlib.axes.Axis): The visualization is displayed on this
            axis. If this is :obj:`None` (default), a new axis is created.

    Returns:
        ~matploblib.axes.Axes:
        Returns the Axes object with the plot for further tweaking.

    """
    from matplotlib import pyplot as plot
    if ax is None:
        fig = plt.figure()
        ax = Axes3D(fig)

    # separate X, Y, Z
    # xArray = np.array( [] )
    # yArray = np.array( [] )
    # zArray = np.array( [] )
    # for point in points:
    #     xArray.append(xArray, point[0])
    #     yArray.append(yArray, point[1])
    #     zArray.append(zArray, point[2])
    # plot
    # ax.scatter3D(np.ravel(X),np.ravel(Y),np.ravel(Z))

    # x = points[:, 0, :]
    # y = points[:, 1, :]
    # z = points[:, 2, :]
    x = points[:, 0]
    y = points[:, 1]
    z = points[:, 2]

    # ax.scatter3D(x, y, z, s=1, c=)
    # pointsize : s=20 (default)
    ax.scatter3D(x, y, z, s=1)

    ax.set_xlabel('X axis')
    ax.set_ylabel('Y axis')
    ax.set_zlabel('Z axis')

    # points
    return ax


def vis_voxelcloud(voxels, ax=None):
    """Visualize a voxel cloud.

    Args:
        points (~numpy.ndarray): An array of shape :math:`(x, y, z)`.
            This is in  format PointCloud and the range of its value is
            :math:`[-∞, +∞]`. (float32?)
        ax (matplotlib.axes.Axis): The visualization is displayed on this
            axis. If this is :obj:`None` (default), a new axis is created.

    Returns:
        ~matploblib.axes.Axes:
        Returns the Axes object with the plot for further tweaking.

    Example:

    """
    from matplotlib import pyplot as plot
    if ax is None:
        fig = plt.figure()
        ax = fig.gca(projectsion='3d')

    # separate X, Y, Z
    xArray = np.array( [] )
    yArray = np.array( [] )
    zArray = np.array( [] )
    for voxel in voxels:
        xArray.append(xArray, voxel[0])
        yArray.append(yArray, voxel[1])
        zArray.append(zArray, voxel[2])

    # voxel
    ax.voxels(voxels, facecolors=colors, edgecolor='k')
    return ax


### pcl + vtk? ###

def vis_pointcloud_pcl(points, ax=None):
    try:
        import pcl
    except:
        print("Cannot import pcl")
    try:
        import pcl.pcl_visualization
    except:
        print("Cannot import pcl.pcl_visualization")

    pt = pcl.PointCloud(points)
    visual = pcl.pcl_visualization.CloudViewing()
    visual.ShowMonochromeCloud(pt)

    flag = True
    while flag:
        # key Param?
        # flag != visual.WasStopped()
        flag = not(visual.WasStopped())


def vis_pointcloud_pcl2(points, colors):
    try:
        import pcl
    except:
        print("Cannot import pcl")
    try:
        import pcl.pcl_visualization
    except:
        print("Cannot import pcl.pcl_visualization")

    rgbpoints = []
    visual = pcl.pcl_visualization.CloudViewing()
    for point, color in zip(points, colors):
        print(point)
        print(color)
        # rgbpoints.append(np.concatenate(point, color))
        # fcolor = np.array(color, dtype=np.float32)
        print(np.append(point, color))
        rgbpoints.append(np.append(point, color))

    rgbpoints = np.array(rgbpoints, dtype=np.float32)
    print(rgbpoints)
    pt = pcl.PointCloud_PointXYZRGB(rgbpoints)
    # set properties(color/pointsize)
    # view
    visual.ShowColorCloud(pt)

    flag = True
    while flag:
        # key Param?
        flag = not(visual.WasStopped())


def vis_voxelcloud_pcl(voxels):
    try:
        import pcl
    except:
        print("Cannot import pcl")
    try:
        import pcl.pcl_visualization
    except:
        print("Cannot import pcl.pcl_visualization")
    pass


def vis_voxelcloud_pcl2(voxels, colors):
    try:
        import pcl
    except:
        print("Cannot import pcl")
    try:
        import pcl.pcl_visualization
    except:
        print("Cannot import pcl.pcl_visualization")
    pass



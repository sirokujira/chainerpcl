import pkg_resources

from chainerpcl import chainer_experimental  # NOQA
from chainerpcl import datasets  # NOQA
from chainerpcl import evaluations  # NOQA
# from chainerpcl import experimental  # NOQA
from chainerpcl import extensions  # NOQA
from chainerpcl import functions  # NOQA
from chainerpcl import links  # NOQA
from chainerpcl import transforms  # NOQA
from chainerpcl import utils  # NOQA
from chainerpcl import visualizations  # NOQA
__version__ = pkg_resources.get_distribution('chainerpcl').version

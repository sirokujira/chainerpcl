[![](docs/images/logo.png)](http://chainerpcl.readthedocs.io/en/stable/)

[![PyPI](https://img.shields.io/pypi/v/chainerpcl.svg)](https://pypi.python.org/pypi/chainerpcl)
[![License](https://img.shields.io/github/license/chainer/chainerpcl.svg)](https://github.com/chainer/chainerpcl/blob/master/LICENSE)
[![travis](https://travis-ci.org/chainer/chainerpcl.svg?branch=master)](https://travis-ci.org/chainer/chainerpcl)
[![Read the Docs](https://media.readthedocs.org/static/projects/badges/passing.svg)](http://chainerpcl.readthedocs.io/en/latest/?badge=latest)

# ChainerPCL: a Library for Deep Learning in Computer Vision

ChainerPCL is a collection of tools to train and run neural networks for computer vision tasks using [Chainer](https://github.com/chainer/chainer).

You can find the documentation [here](http://chainerpcl.readthedocs.io/en/latest/).

Supported tasks:

# pcl classification img
![Example are outputs of detection models supported by chainerpcl](https://cloud.githubusercontent.com/assets/2062128/26337670/44a2a202-3fb5-11e7-8b88-6eb9886a9915.png)
These are the outputs of the detection models supported by chainerpcl.

# Guiding Principles
ChainerPCL is developed under the following three guiding principles.

+ **Ease of Use** -- Implementations of computer vision networks with a cohesive and simple interface.
+ **Reproducibility** -- Training scripts that are perfect for being used as reference implementations.
+ **Compositionality** -- Tools such as data loaders and evaluation scripts that have common API.

# Installation

```bash
$ pip install -U numpy
$ pip install chainerpcl
```

The instruction on installation using Anaconda is [here](http://chainerpcl.readthedocs.io/en/stable/#install-guide) (recommended).

### Requirements

+ [Chainer](https://github.com/chainer/chainer) and its dependencies
+ Pillow
+ Cython (Build requirements)

For additional features

+ [ChainerMN](https://github.com/chainer/chainermn)
+ Matplotlib
+ OpenCV
+ python-pcl
+ SciPy
+ pykitti
+ laspy

Environments under Python 2.7.12 and 3.6.0 are tested.

+ The master branch is designed to work on Chainer v4 (the stable version) and v5 (the development version).

# Data Conventions

+ Image
  + The order of color channel is RGB.
  + Shape is CHW (i.e. `(channel, height, width)`).
  + The range of values is `[0, 255]`.
  + Size is represented by row-column order (i.e. `(height, width)`).
+ Bounding Boxes
  + Shape is `(R, 4)`.
  + Coordinates are ordered as `(y_min, x_min, y_max, x_max)`. The order is the opposite of OpenCV.
+ Semantic Segmentation Image
  + Shape is `(height, weight)`. 
  + The value is class id, which is in range `[0, n_class - 1]`.

# Sample Visualization

![Example are outputs of detection models supported by ChainerCV](https://cloud.githubusercontent.com/assets/2062128/26337670/44a2a202-3fb5-11e7-8b88-6eb9886a9915.png)
These are the outputs of the detection models supported by ChainerCV.


